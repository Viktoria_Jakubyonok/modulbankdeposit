﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;

namespace Курсовая
{
    public partial class AddEmployee : Form
    {
        ArrayList JobList = new ArrayList();
        public AddEmployee()
        {
            InitializeComponent();

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";

            cn.Open();

            string strSelectPosition = "Select  [Employee's_position].Job_name, [Employee's_position].Job_code from [Employee's_position] where Department_code = 3";
            SqlCommand cmdSelectPosition = new SqlCommand(strSelectPosition, cn);
            SqlDataReader typeReader = cmdSelectPosition.ExecuteReader();


            Deposit.ComboboxAddFromBase(typeReader, JobList, comboBox1);
            cn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";

            cn.Open();
            try
            {
                int id_empl = Employee.EmployeeList[Employee.EmployeeList.Count - 1].Id + 1;

                string name = null;
                string surname = null;
                string patronymic = null;
                string phone = null;
                if ((textBox1.Text == "") && (textBox2.Text == "") && (textBox3.Text == "") && (textBox5.Text == ""))
                {
                    MessageBox.Show("Все поля обязательны для заполнения!");
                }
                else
                {
                    name = textBox2.Text;
                    surname = textBox1.Text;
                    patronymic = textBox3.Text;
                    phone = textBox5.Text;
                }
                 
                  DateTime birth = Convert.ToDateTime(textBox4.Text);              

                string job_for_list = comboBox1.SelectedItem.ToString();//для листа название должности

                int job = Deposit.SearchCodefromCombobox(JobList, comboBox1);

                string qualific = "";//образование
                if (radioButton2.Checked == true)
                {
                    qualific = radioButton2.Text;
                }
                else
                {
                    qualific = radioButton1.Text;
                }


                string strInsertNewEmployee = string.Format("INSERT INTO Employees VALUES ('{0}','{1}','{2}','{3}','{4}','{5}', '{6}', '{7}')", id_empl, surname, name, patronymic, phone, birth, job, qualific);

                SqlCommand cmdInsertNewEmployee = new SqlCommand(strInsertNewEmployee, cn);
                cmdInsertNewEmployee.ExecuteNonQuery();

                Job jobaddempl = new Job(job, "Отдел вкладов", job_for_list, 8); //значение departmen_name и workday - прописываю вручную, т.к. не успеваю сделать основную часть, в дальнейшем изменю запрос и буду считывать эту инфу также.


                Employee employee = new Employee(id_empl, name, surname, patronymic, birth, jobaddempl, phone);

                label9.Text = Convert.ToString(employee.Id);
            }
            catch
            {
                MessageBox.Show("Проверьте корректность введенных данных!\nВозможные ошибки:\nНекорректная дата рождения, вид: 01.01.2000.\nНеобходимо выбрать образование и квалификацию.");
            }

            cn.Close();
        }
    }
}
