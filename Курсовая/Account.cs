﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Курсовая
{
    class Account
    {
        private int idacc;
        private decimal balance;

        public Account (int idacc, decimal balance)
        {
            this.balance = balance;
            this.idacc = idacc;
        }

        public int Idacc
        {
            get { return idacc; }
            set { idacc = value; }
        }
        public decimal Balance
        {
            get { return Math.Round(balance); }
            set { balance = value; }
        }

        public virtual string Info()//виртуальный метод
        {
            string s = "№ Счета" + Idacc +
                "\nБаланс: " + Balance +
                 "\n";
            return s;
        }
       

    }
}
