﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;

namespace Курсовая
{
  public  class TypeDeposit
    {
        private string name;
        private string typeDep;
        private int day;
        private string currency;
        private decimal proccent;
        private int id;
        public static List<TypeDeposit> TypeDepositList = new List<TypeDeposit>();
        

        public TypeDeposit ( int id,string name, string typeDep, int day, string currency, decimal proccent)
        {
            this.id = id;
            this.name = name;
            this.typeDep = typeDep;
            this.day = day;
            this.currency = currency;
            this.proccent = proccent;
            TypeDepositList.Add(this);
        }

        public TypeDeposit(int id, string name, string typeDep,  string currency, decimal proccent)
        {
            this.id = id;
            this.name = name;
            this.typeDep = typeDep;
            this.currency = currency;
            this.proccent = proccent;
            TypeDepositList.Add(this);
        }


        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string TypeDep
        {
            get { return typeDep; }
            set { typeDep = value; }
        }

        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        public int Day
        {
            get { return day; }
            set { day = value; }
        }

        public decimal Proccent
        {
            get { return proccent; }
            set { proccent = value; }
        }

        public static void SearchType (DataGridView dt, TextBox textbox, int cells)//метод поиска информации о типах вклада по номеру вклада
        {
            for (int i = 0; i < dt.Rows.Count; i++) //при повторном поиске будет окрашиваться сначала вся сетка в прежний цвет
            {
                    dt.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }

            string id_name = "";
            string type = "";

            int f = 0;
            for (int i =0; i<TypeDepositList.Count; i++) //организация поиска по спиcку
            {
                if (TypeDepositList[i].ID.ToString() == textbox.Text)
                {
                    id_name = TypeDepositList[i].ID.ToString();
                    type = TypeDepositList[i].TypeDep.ToString();
                    string stringMessageBox = "Название: " + TypeDepositList[i].Name + "\nТип: " + TypeDepositList[i].TypeDep + "\nВалюта: " + TypeDepositList[i].Currency + "\nПроцентная ставка: " + TypeDepositList[i].Proccent + "\nСрок: " + TypeDepositList[i].Day + " дней";
                    MessageBox.Show(stringMessageBox);
                    f = 1;
                    break;
                }
            }
            if(f==0)
            {
                MessageBox.Show("Данного типа вклада не существует!\nПроверьте корректность введенных данных.");
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string l = Convert.ToString(dt.Rows[i].Cells[cells].Value);
                if (id_name == l )
                {
                    dt.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                }

            }

        }

      public static void ProccentSort () //сортировка списка по процентной ставке (в программе не рекомендована к использованию!!!), написан для наглядности и показания умений
  {
       var sortedProccent = from u in TypeDepositList
                  orderby u.Proccent
                  select u;
       string str = "";
       foreach (TypeDeposit u in sortedProccent)
       {
           str = "Вклад: " + u.Name + "Процент: " + u.Proccent + "      Код: " + u.ID + "\n" +  str;
          
       }
       MessageBox.Show(str);
  }
       

        
       

    
      

           
        }

    }

