﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;

namespace Курсовая
{

    public partial class Form1 : Form
    {
        public MainWindow fmainwindow; //подчиненная форма
        public AddEmployee addempl;
        public Openning oppen;
        public Form1()
        {

            InitializeComponent();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";
            cn.Open();

            string strSelectEmployee = "Select Employees.Employee_code, Employees.Name, Employees.Surname, Employees.Patronymic, Employees.Date_of_Birth, [Employee's_position].Job_code, Divisions.Department_name, [Employee's_position].Job_name, [Employee's_position].Working_day, Employees.Phone_number from Employees inner join [Employee's_position] on Employees.Job_code = [Employee's_position].Job_code inner join Divisions on [Employee's_position].Department_code = Divisions.Department_code";
            SqlCommand cmdSelectEmployee = new SqlCommand(strSelectEmployee, cn);
            SqlDataReader typeReader = cmdSelectEmployee.ExecuteReader();


            int id_empl = 0;
            while (typeReader.Read())
            {
                id_empl = typeReader.GetInt32(0);
                string name = typeReader.GetString(1);
                string surname = typeReader.GetString(2);
                string patronymic = typeReader.GetString(3);
                DateTime birth = typeReader.GetDateTime(4);
                int id_jobs = typeReader.GetInt32(5);
                string department = typeReader.GetString(6);
                string name_jobs = typeReader.GetString(7);
                int workday = typeReader.GetInt32(8);
                string phone = typeReader.GetString(9);

                Job job = new Job(id_jobs, department, name_jobs, workday);


                Employee employee = new Employee(id_empl, name, surname, patronymic, birth, job, phone);

            }
            typeReader.Close();
            cn.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            int id_employess = Convert.ToInt32(textBox1.Text);
            IdEmployee.Value = id_employess.ToString();
            Employee.SearchEmployee(textBox1, id_employess, fmainwindow);//вызов метода для поиска сотрудника в списке  и перехода к новому окну, если он найден
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            addempl = new AddEmployee();
            addempl.ShowDialog();
            
        }
    }
    static class IdEmployee
    {
        public static string Value { get; set; }
    }
}
