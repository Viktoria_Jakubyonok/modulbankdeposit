﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace Курсовая
{
    class Client:Person
    {
        private Contacts contacts;
        public string passport;
        public static List<Client> ClientList = new List<Client>();
        
        public Client (int id, string surname, string name, string patronymic, DateTime birthday, string passport, Contacts contacts )
            : base (id, surname, name, patronymic, birthday)
        {
            this.contacts = contacts;
            this.passport = passport;
            ClientList.Add(this);
            
        }

        public string Passport
        {
            get { return passport; }
            set { passport = value; }
        }

        public Contacts Contacts
        {
            get { return contacts; }
            set { contacts = value; }
        }

        public override void Display()//пока что нигде не задействован
        {
            // Используется ссылка на метод, определенный в базовом классе Person
            MessageBox.Show("Имя: " + Name + "\nФамилия: " + Surname + "\nКод клиента: " + Id + "\nПасспорт: " + Passport);
        }
    }
}
