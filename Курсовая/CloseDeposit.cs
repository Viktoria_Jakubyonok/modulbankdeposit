﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Курсовая
{
    public partial class CloseDeposit : Form
    {
        public CloseDeposit()
        {
            InitializeComponent();
            textBox1.Clear();       
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label5.Text = "";
            label6.Text = "";
            label7.Text = "";
            int f = 0; //по этой переменной будем выводить сообщение;
                for (int i = 0; i < Deposit.DepositList.Count; i++)
                {
                    if (textBox1.Text == Deposit.DepositList[i].Idacc.ToString() && Deposit.DepositList[i].Type.TypeDep == "до востребования    " && Deposit.DepositList[i].Status == "активен   ")
                    {
                        label5.Text = (Deposit.DepositList[i].Balance + Deposit.DepositList[i].AddProccent).ToString(); //пока что без процентов
                        label6.Text = Deposit.DepositList[i].AddProccent.ToString();
                        label7.Text = Deposit.DepositList[i].Type.Currency;
                        f = 1;
                        break;

                    }
                    
                }
            if (f==0)
            {
                MessageBox.Show("Данный вклад не найден \nДанная операция не возможно для срочных вкладов");
                textBox1.Clear();
            }
            else
            {
                MessageBox.Show("Вклад успешно найден!");
            }        
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
             "Integrated Security = SSPI;" +
             "Initial Catalog = OOP";
            cn.Open();

            for (int i = 0; i < Deposit.DepositList.Count; i++)
            {
                if (textBox1.Text == Deposit.DepositList[i].Idacc.ToString() && Deposit.DepositList[i].Type.TypeDep == "до востребования    " && Deposit.DepositList[i].Status == "активен   ")
                {
                    Deposit.DepositList[i].Status = "пассивен  ";
                    Deposit.DepositList[i].DateClose = DateTime.Now;
                    int id_statusforBD = 2;

                    string strUpdateDeposit = string.Format("UPDATE Credits_Deposits SET status = @status, Date_of_redemption_closing =@date, Bank_rate=@rate where Credit_Deposit_number = @idacc");
                    SqlCommand cmdUpdateDeposit = new SqlCommand(strUpdateDeposit, cn);
                    // создаем параметр для имени
                    SqlParameter statusParam = new SqlParameter("@status", id_statusforBD);
                    // добавляем параметр к команде
                    cmdUpdateDeposit.Parameters.Add(statusParam);
                    SqlParameter idaccParam = new SqlParameter("@idacc", Deposit.DepositList[i].Idacc);
                    // добавляем параметр к команде
                    cmdUpdateDeposit.Parameters.Add(idaccParam);
                    SqlParameter datecloseParam = new SqlParameter("@date", DateTime.Now);
                    // добавляем параметр к команде
                    cmdUpdateDeposit.Parameters.Add(datecloseParam);
                    SqlParameter rateParam = new SqlParameter("@rate", Deposit.DepositList[i].AddProccent);
                    // добавляем параметр к команде
                    cmdUpdateDeposit.Parameters.Add(rateParam);
                     cmdUpdateDeposit.ExecuteNonQuery();
                     MessageBox.Show("Данный депозит закрыт.");


                }              

            }
        }
    }
}
