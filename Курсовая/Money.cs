﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Курсовая
{
    class Money
    {
        private long mon; //long - т.к. самый большой тип данных для работы с целочисленными числами, а деньги могут быть выражены в разных суммах

        //конструкторы
        public Money (double mon) 
        {
            this.mon = (long)Math.Round(100 * mon, 2);
        }

        public Money (long first, int second)
        {
            if (second < 0 || second > 99) throw new ArgumentException();// генерируем исключение
            if (first >= 0)
                mon = 100 * first + second;
            else mon = 100 * first - second;
        }

        public Money (int copecks) // конструктор, с помощью которого будем организовано проведение внутренних операция
        {
            this.mon = copecks;
        }

        //свойства
        public long First //количество рублей
        {
            get { return mon / 100; }
        }

        public int Second //количество копеек
        {
            get { return (int)(mon % 100); }
        }

        //Умножение
        public Money Multiplay(double mon)
        {
            return new Money((long)Math.Round(this.mon * mon, 2));
        }

        public  static Money operator* (double a, Money b)
        { 
            return new Money((long)Math.Round(a * b.mon, 2)); 
        }

        public static Money operator* (Money a, double b)
        {
            return new Money((long)Math.Round(a.mon * b, 2));
        }

        //Деление
        public Money Division(double mon)
        {
            return new Money((long)Math.Round(this.mon / mon, 2));
        }

        public static Money operator/ (double a, Money b)
        {
            return new Money((long)Math.Round(a / b.mon, 2));
        }

        public static Money operator/ (Money a, double b)
        {
            return new Money((long)Math.Round(a.mon / b, 2));
        }

        //Сложение
        public Money Addition(double mon)
        {
            return new Money((long)(this.mon + mon));
        }

        public static Money operator+ (double a, Money b)
        {
            return new Money((long)(a + b.mon));
        }

        public static Money operator+ (Money a, double b)
        {
            return new Money((long)(a.mon +b));
        }

        //Вычитание
        public Money Subtraction(double mon)
        {
            return new Money((long)(this.mon - mon));
        }

        public static Money operator- (double a, Money b)
        {
            return new Money((long)(a - b.mon));
        }

        public static Money operator- (Money a, double b)
        {
            return new Money((long)(a.mon - b));
        }

        public static Money operator++(Money a) //поддержка унарного оператора
        {
            return new Money(a.mon++);
        }

   

    }
}
