﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using Excel = Microsoft.Office.Interop.Excel;

namespace Курсовая
{
    public partial class MainWindow : Form
    {
        TypeDep tpd = new TypeDep();
        Openning opn = new Openning();
        CloseDeposit clp = new CloseDeposit();

        public MainWindow()
        {
            InitializeComponent();

              SqlConnection cn = new SqlConnection();

            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";
            cn.Open();

            string strSelectdeposit = "select Credits_Deposits.Client_code, Date_of_issue_opening, ISNULL(Date_of_redemption_closing, '1773-01-01 00:00:00.000'), Amount, Credit_Deposit_number, Loan_Deposits_type_code, Status.name, Bank_rate from Credits_Deposits inner join Status on Credits_Deposits.status = Status.id_status";
            SqlCommand cmdSelectdeposit = new SqlCommand(strSelectdeposit, cn);
            SqlDataReader depositReader = cmdSelectdeposit.ExecuteReader();

            while (depositReader.Read())
            {
                int id_client = depositReader.GetInt32(0);
                DateTime date_open = depositReader.GetDateTime(1);
                DateTime date_close = depositReader.GetDateTime(2);
                decimal balance = depositReader.GetDecimal(3);
                int id_depositNumber = depositReader.GetInt32(4);
                int id_type = depositReader.GetInt32(5);
                string id_status = depositReader.GetString(6);
                decimal addrate = depositReader.GetDecimal(7);

                Client p = null;
                for (int i =0; i<Client.ClientList.Count; i++)
{       
                    if (id_client == Client.ClientList[i].Id)
                    p = Client.ClientList[i];
}

                TypeDeposit t = null;
                for (int i = 0; i < TypeDeposit.TypeDepositList.Count; i++)
                {
                    if (id_type == TypeDeposit.TypeDepositList[i].ID)
                        t = TypeDeposit.TypeDepositList[i];
                }



                Deposit deposit = new Deposit(addrate, p, date_open, date_close, balance, id_depositNumber, t, id_status);
                deposit.StatusChange();
                if (deposit.Type.TypeDep == "срочный             ")
                {
                    deposit.SimpleProccent();
                }
                else
                {
                        deposit.ComplexProccent();               
                }
       
            }

            depositReader.Close();


            for (int i = 0; i < Deposit.DepositList.Count; i++)//добавление новых подсчитаных данных в таблицу БД
            {
                int id_statusforBD = 0;
                if (Deposit.DepositList[i].Status == "пассивен  ")
                {
                    id_statusforBD = 2;
                }
                else
                {
                    id_statusforBD = 1;
                }

                string strUpdateDeposit = string.Format("UPDATE Credits_Deposits SET Bank_rate = @rate, status = @status where Credit_Deposit_number = @idacc");
                SqlCommand cmdUpdateDeposit = new SqlCommand(strUpdateDeposit, cn);
                // создаем параметр для имени
                SqlParameter rateParam = new SqlParameter("@rate", Deposit.DepositList[i].AddProccent);
                // добавляем параметр к команде
                cmdUpdateDeposit.Parameters.Add(rateParam);
                // создаем параметр для возраста
                SqlParameter statusParam = new SqlParameter("@status", id_statusforBD);
                // добавляем параметр к команде
                cmdUpdateDeposit.Parameters.Add(statusParam);
                SqlParameter idaccParam = new SqlParameter("@idacc", Deposit.DepositList[i].Idacc);
                // добавляем параметр к команде
                cmdUpdateDeposit.Parameters.Add(idaccParam);

                cmdUpdateDeposit.ExecuteNonQuery();
            }

         
           
        }

        
       
        private void Файл_Opening(object sender, CancelEventArgs e)
        {
        }

        private void label2_Click(object sender, EventArgs e)
        {   
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

            for (int i = 0; i < Deposit.DepositList.Count; i++) //добавление в сетку информации о вкладах банка
            {
                string columns_1 = Deposit.DepositList[i].Client.Surname;//
                string columns_2 = Deposit.DepositList[i].Client.Name;
                string columns_3 = Deposit.DepositList[i].Client.Patronymic;
                string columns_4 = Deposit.DepositList[i].Idacc.ToString();
                string columns_5 = Deposit.DepositList[i].Client.Passport;
                string columns_6 = Deposit.DepositList[i].Type.Name;
                string columns_7 = Deposit.DepositList[i].Balance.ToString();
                string columns_8 = Deposit.DepositList[i].DateOppened.ToString();
                string columns_9 = Deposit.DepositList[i].Day().ToString();

                string columns_10 = Deposit.DepositList[i].AddProccent.ToString();//сумма с добавленным процентом
                string columns_11 = (Deposit.DepositList[i].AddProccent + Deposit.DepositList[i].Balance).ToString();//чисто проценты
                string columns_12 = Deposit.DepositList[i].Status;
                string columns_13 = Deposit.DepositList[i].Type.Currency;


                dataGridView1.Rows.Add(columns_1, columns_2, columns_3, columns_4, columns_5, columns_6, columns_7, columns_13, columns_8, columns_9, columns_10, columns_11, columns_12);

            }
            label10.Text = TypeDeposit.TypeDepositList.Count.ToString();
            label11.Text = Deposit.AmmountProccent().ToString();
            label12.Text = Deposit.CountDeposit("пассивен  ").ToString();
            label13.Text = Deposit.CountDeposit("активен   ").ToString();

        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void видВкладаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tpd.ShowDialog();
        }

        private void выйтиИзСистемыToolStripMenuItem_Click(object sender, EventArgs e)
        {
           this.Close();
        }

        private void открытиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            opn.ShowDialog();
        }

        private void пополнениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void закрытиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clp.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)//кнопка обновления информации
        {
            dataGridView1.Rows.Clear();
            for (int i = 0; i < Deposit.DepositList.Count; i++) //добавление в сетку информации о вкладах банка
            {
                string columns_1 = Deposit.DepositList[i].Client.Surname;//
                string columns_2 = Deposit.DepositList[i].Client.Name;
                string columns_3 = Deposit.DepositList[i].Client.Patronymic;
                string columns_4 = Deposit.DepositList[i].Idacc.ToString();
                string columns_5 = Deposit.DepositList[i].Client.Passport;
                string columns_6 = Deposit.DepositList[i].Type.Name;
                string columns_7 = Deposit.DepositList[i].Balance.ToString();
                string columns_8 = Deposit.DepositList[i].DateOppened.ToString();
                string columns_9 = Deposit.DepositList[i].Day().ToString();
                string columns_10 = Deposit.DepositList[i].AddProccent.ToString();//сумма процентами
                string columns_11 = Deposit.DepositList[i].Result().ToString();//общая сумма
                string columns_12 = Deposit.DepositList[i].Status;
                string columns_13 = Deposit.DepositList[i].Type.Currency;


                dataGridView1.Rows.Add(columns_1, columns_2, columns_3, columns_4, columns_5, columns_6, columns_7, columns_13, columns_8, columns_9, columns_10, columns_11, columns_12);

            }
            label10.Text = TypeDeposit.TypeDepositList.Count.ToString();
            label11.Text = Deposit.AmmountProccent().ToString();
            label12.Text = Deposit.CountDeposit("пассивен  ").ToString();
            label13.Text = Deposit.CountDeposit("активен   ").ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Deposit.SearchAccount(dataGridView1, textBox1, 3);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label5.Text = DateTime.Now.ToString("h:mm:ss.fff");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Excel.Application excel = new Excel.Application();
            excel.SheetsInNewWorkbook = 8;//число листов в книге
            excel.Workbooks.Add(Type.Missing);//тип шаблона эксель, местный аналог стандартных значений
            Excel.Workbook workbook = excel.Workbooks[1];//файл рабочей книги
            Excel.Worksheet sheet = workbook.Worksheets.get_Item(1);//рабочий лист
            sheet.Cells[1, 1].Value = "Фамилия";
            sheet.Cells[1, 2].Value = "Имя";
            sheet.Cells[1, 3].Value = "Отчество";
            sheet.Cells[1, 4].Value = "Счет";
            sheet.Cells[1, 5].Value = "Серийный номер";
            sheet.Cells[1, 6].Value = "Вид вклада";
            sheet.Cells[1, 7].Value = "Сумма вклада";
            sheet.Cells[1, 8].Value = "Валюта";
            sheet.Cells[1, 9].Value = "Дата открытия";
            sheet.Cells[1, 10].Value = "Прошедший срок";
            sheet.Cells[1, 11].Value = "Проценты";
            sheet.Cells[1, 12].Value = "Общая сумма";
            sheet.Cells[1, 13].Value = "Статус";


            for (int i = 1; i <= this.dataGridView1.ColumnCount; i++)
            {
                for (int j = 2; j <= this.dataGridView1.RowCount; j++)
                {
                    sheet.Cells[j, i].Value = this.dataGridView1[i - 1, j - 2].Value.ToString();//запись в ячейки Excel данных из таблицы
                }
            }
            excel.Visible = true;//видимость
            sheet.Cells.get_Range("A1", "M" + Convert.ToInt32(this.dataGridView1.RowCount)).Borders.LineStyle = Excel.XlLineStyle.xlContinuous;//диапазон для записи данных одной строки
            sheet.Cells.get_Range("A1", "M" + Convert.ToInt32(this.dataGridView1.RowCount)).Borders.Weight = Excel.XlBorderWeight.xlThick;//линия границы таблицы Excel

        }
    }
}
