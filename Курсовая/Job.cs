﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Курсовая
{
    class Job
    {
        private int id_jobs;
        private string department;
        private string name_jobs;
        private int workday;

        public Job (int id_jobs, string department, string name_jobs,  int workday)
        {
            this.id_jobs = id_jobs;
            this.department = department;
            this.name_jobs = name_jobs;
            this.workday = workday;
        }

        public int Id_jobs
        {
            get { return id_jobs; }
        }

        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        public string Name_jobs
        {
            get { return name_jobs; }
            set { name_jobs = value; }
        }

        public int Workday
        {
            get { return workday; }
            set { workday = value; }
        }



    }
}
