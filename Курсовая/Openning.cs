﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;

namespace Курсовая
{
    public partial class Openning : Form
    {
        ArrayList CityList2 = new ArrayList();//список для городов и их кодов
        //ArrayList TypeList = new ArrayList();//список для типа вклада
        ArrayList DepositType = new ArrayList();//список для вида вклада

        public Openning()
        {
            InitializeComponent();


            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";
            cn.Open();

            string strSelectClient = "Select Client_code, Surname, Name, Patronymic, Date_of_Birth, Passport_series, Region.Region_name, City.City_name, Street,House, Phone_number from Clients inner join City on City.City_code=Clients.City inner join Region on Region.Region_code = City.Region_code";
            SqlCommand cmdSelectClient = new SqlCommand(strSelectClient, cn);
            SqlDataReader typeReader = cmdSelectClient.ExecuteReader();

            while (typeReader.Read())
            {
                int id = typeReader.GetInt32(0);
                string surname = typeReader.GetString(1);
                string name = typeReader.GetString(2);
                string patronymic = typeReader.GetString(3);
                DateTime birth = typeReader.GetDateTime(4);
                string passport = typeReader.GetString(5);
                string region = typeReader.GetString(6);
                string town = typeReader.GetString(7);
                string street = typeReader.GetString(8);
                string house = typeReader.GetString(9);
                string phone = typeReader.GetString(6);

                City city = new City(region, town);
                Address address = new Address(street, house, city);
                Contacts contacts = new Contacts(address, phone);

                Client client = new Client(id, surname, name, patronymic, birth, passport, contacts);
            }


            typeReader.Close();

            string strSelectCity = "select City_name, City_code from City";
            SqlCommand cmdSelectCity = new SqlCommand(strSelectCity, cn);
            SqlDataReader cityReader = cmdSelectCity.ExecuteReader();

            Deposit.ComboboxAddFromBase(cityReader, CityList2, comboBox2); //запись НАЗВАНИЯ ГОРОДОВ в comboBox

            cityReader.Close();

            string strSelectTypeDep = "select  Name_type, Loan_Deposits_type_code FROM Types_of_credits_deposits";
            SqlCommand cmdSelectTypeDep = new SqlCommand(strSelectTypeDep, cn);
            SqlDataReader typedepositReader = cmdSelectTypeDep.ExecuteReader();

            Deposit.ComboboxAddFromBase(typedepositReader, DepositType, comboBox3);//запись ВИДА ВКЛАДА в comboBox
            typedepositReader.Close();

            //считывание вкладов имеющихся в банке (открытых и закрытых)
            string strSelectdeposit = "select Credits_Deposits.Client_code, Date_of_issue_opening, Date_of_redemption_closing, Amount, Credit_Deposit_number, Loan_Deposits_type_code, Status.name, Bank_rate from Credits_Deposits inner join Status on Credits_Deposits.status = Status.id_status";
            SqlCommand cmdSelectdeposit = new SqlCommand(strSelectdeposit, cn);
            SqlDataReader depositReader = cmdSelectdeposit.ExecuteReader();



            cn.Close();




        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
       

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";

            cn.Open();



                int id_client = Client.ClientList[Client.ClientList.Count - 1].Id + 1;//расчтет кода клиента
                int id_deposit = Deposit.DepositList[Deposit.DepositList.Count - 1].Idacc + 1;//расчет номера счета
 
                DateTime birth = new DateTime(1753,1,1);
                try
                {
                    birth = Convert.ToDateTime(textBox6.Text);
                }
            catch
                {
                    MessageBox.Show("Введите возраст в формате: 01.01.2001");
                }


                string CityForList = "";
                int city = 0;
                try
                {
                    CityForList = comboBox2.SelectedItem.ToString();//для листа
                    city = Deposit.SearchCodefromCombobox(CityList2, comboBox2); //для нахождение в Листе кода города по его имени
                }
            catch
                {
                    MessageBox.Show("Выберите город!");
                }


                City cityForClass = null;
                for (int i = 0; i < City.CityList.Count; i++)
                {
                    if (City.CityList[i].Town == CityForList)
                    {
                        cityForClass = City.CityList[i];
                    }
                }

                string surname = null;
                string name = null;
                string patronymic = null;
                string phone = null;
                string passport = null;
                string street = null;
                string home = null;
                decimal amount = 0;
                Client client = null;
                if ((textBox1.Text == "") && (textBox2.Text == "") && (textBox3.Text == "") && (textBox4.Text == "") && (textBox5.Text == "") && (textBox8.Text == "") && (textBox9.Text == "") && (textBox10.Text == ""))
                {
                    MessageBox.Show("Все поля обязательны для заполнения!");
                }
                else
                {
                    name = textBox2.Text;
                    surname = textBox1.Text;
                    patronymic = textBox3.Text;
                    phone = textBox4.Text;
                    passport = textBox5.Text;
                    street = textBox8.Text;
                    home = textBox9.Text;
                    amount = Convert.ToDecimal(textBox10.Text);

                    Address address = new Address(street, home, cityForClass);
                    Contacts contacts = new Contacts(address, phone);
                    client = new Client(id_client, surname, name, patronymic, birth, passport, contacts); //добавление нового клиента в класс

                    if (client.Age() < 18)
                    {
                        Client.ClientList.RemoveAt(Client.ClientList.Count - 1);
                        MessageBox.Show("Данный клиент не добавлен  в базу данных банка!!! Возраст меньше 18 лет!");
                    }
                    else
                    {
                        //добавляем в базу нового клиента
                        string strInsertNewClient = string.Format("INSERT INTO Clients VALUES ('{0}','{1}','{2}','{3}','{4}','{5}', '{6}', '{7}', '{8}', '{9}')", id_client, surname, name, patronymic, birth, passport, city, street, home, phone);
                        SqlCommand cmdInsertNewClient = new SqlCommand(strInsertNewClient, cn);
                        cmdInsertNewClient.ExecuteNonQuery();
                    }

                }

                try
                {
                    string TypeDepForList = comboBox3.SelectedItem.ToString();//для листа
                }
            catch
                {
                    MessageBox.Show("Не выбран вклад!!!");
                }
                int typedep = Deposit.SearchCodefromCombobox(DepositType, comboBox3); //для нахождение в Листе кода вида вклада по его имени

                DateTime date = DateTime.Now;//сегодняшняя дата

                int day = 0;
                decimal proccent = 0;
                string currency = "";//валюта


                for (int i = 0; i < TypeDeposit.TypeDepositList.Count; i++)// по номеру вклада забираем иЗ  листа с видами вклада инфу о сроке и процентной ставке
                {
                    if (TypeDeposit.TypeDepositList[i].ID == typedep)//ищем тип вклада в списке доступных типов вклада
                    {
                        if (TypeDeposit.TypeDepositList[i].Day != 0)//если срок не равен 0 значит это срочный вклад и имеет установленный срок
                        {
                            day = TypeDeposit.TypeDepositList[i].Day;//назначаем срок срочному вкладу
                            MessageBox.Show(day.ToString());
                        }
                        currency = TypeDeposit.TypeDepositList[i].Currency;//валюта для всех вкладов
                    }
                }

                DateTime dateClose = date.AddDays(Convert.ToDouble(day));//дата закрытия для срочного вклада, к дате добавляется количество дней

 
                Deposit deposit = null;
                for (int i = 0; i < TypeDeposit.TypeDepositList.Count; i++)
                {
                    if (TypeDeposit.TypeDepositList[i].ID == typedep && client != null)//если данный вклад находим в списке доступных вкладов
                    {
                        deposit = new Deposit(proccent, client, date, dateClose, amount, id_deposit, TypeDeposit.TypeDepositList[i], "активен   ");
                      //  MessageBox.Show(Deposit.DepositList[Deposit.DepositList.Count - 1].DateClose.ToString());//создаем объект депозит
                    }

                }

                try
                {
                    string strInsertNewDeposit = "";//ВЫБИРАЕМ ЗАПРОС В ЗАВИСИМОСТИ ОТ ТИПА ВКЛАДА
                    for (int i = 0; i < Deposit.DepositList.Count; i++)
                    {
                        if (Deposit.DepositList[i].Idacc == id_deposit && Deposit.DepositList[i].Type.TypeDep == "срочный             ")//если депозаит срочный и существует в списке, то выполняется вставка в БД
                        {
                            strInsertNewDeposit = string.Format("INSERT INTO Credits_Deposits VALUES ('{0}','{1}','{2}','{3}','{4}','{5}', '{6}', '{7}', '{8}')", id_deposit, typedep, id_client, IdEmployee.Value, amount, date, dateClose, "0", "1");
                        }

                        else
                        {
                            strInsertNewDeposit = string.Format("INSERT INTO Credits_Deposits VALUES ('{0}','{1}','{2}','{3}','{4}','{5}', NULL, '{6}', '{7}')", id_deposit, typedep, id_client, IdEmployee.Value, amount, date, "0", "1");
                        }

                    }

                    SqlCommand cmdInsertNewDeposit = new SqlCommand(strInsertNewDeposit, cn);//запись открытого вклада в бд
                    cmdInsertNewDeposit.ExecuteNonQuery();

                    label20.Text = id_deposit.ToString();
                    MessageBox.Show("Счет успешно открыт!");
                }
            catch
                {
                    MessageBox.Show("Такого клиента в банке нет!");
                }



      
           
        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {
       
        }

       


    }

}

