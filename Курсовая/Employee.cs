﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;

namespace Курсовая
{
    class Employee: Person
    {
        private Job job;
        private string phone;
        public static List<Employee> EmployeeList = new List<Employee>();

        public Employee ( int id_empl, string name, string surname, string patronymic, DateTime birthday, Job job, string phone)
            : base (id_empl, surname, name, patronymic, birthday)
        {
            this.job = job;
            this.phone = phone;
            EmployeeList.Add(this);
        }

        public Job Job
        {
            get { return job; }
            set { value = job; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public static void SearchEmployee(TextBox textbox, int id_employees, Form f) //поиск сотрудника в списке
        {
            int id_employess = Convert.ToInt32(textbox.Text);
            int e = 0;
            for (int i = 0; i < Employee.EmployeeList.Count; i++) //если сотрудник есть и он с валютного отдела, то вход осуществляется !!!!!!вставить исключение
            {
                if (id_employess == Employee.EmployeeList[i].Id && Employee.EmployeeList[i].Job.Department == "Отдел вкладов")
                {
                   f = new MainWindow();
                   f.Show();
                   e = 1;
                    break;
                }
                
            }
            if (e == 0)
            {
                MessageBox.Show("Такого сотрудника нет в ОТДЕЛЕ ВКЛАДОВ");
                textbox.Clear();
            }
            
        }

        public override void Display()//пока что нигде не задействован
        {
            // Используется ссылка на метод, определенный в базовом классе Person
            MessageBox.Show("Имя: " + Name + "\nФамилия: " + Surname + "\nКод сотрудника: " + Id + "\nТелефон: " + Phone);
        }
    }
}
