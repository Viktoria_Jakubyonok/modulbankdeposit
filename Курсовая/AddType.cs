﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;

namespace Курсовая
{
    public partial class AddType : Form
    {
        
        public AddType()
        {
            InitializeComponent();
        }
        ArrayList CurrencyList = new ArrayList();
        private void label9_Click(object sender, EventArgs e)
        {
        }

        private void AddType_Load(object sender, EventArgs e)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";

            cn.Open();

            string strSelectType = "Select Name, Currency_code from Currency";
            SqlCommand cmdSelectType = new SqlCommand(strSelectType, cn);
            SqlDataReader typeReader = cmdSelectType.ExecuteReader();

            Deposit.ComboboxAddFromBase(typeReader, CurrencyList, comboBox1);

            cn.Close();

        }

        private void button1_Click(object sender, EventArgs e) //добавление нового типа вклада
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";

            cn.Open();

              int id = TypeDeposit.TypeDepositList[TypeDeposit.TypeDepositList.Count - 1].ID+1;//задаем код депозиту
              
            
            string name = null;//имя вклада
            
            int day_max = 0;//срок макс
            int day_min = 0;//срок мин
            int amount_min = 0;//сумма мин
            string description = null;//описание
            string currency_for_list = null;//для листа


            int currency = Deposit.SearchCodefromCombobox(CurrencyList, comboBox1); //для нахождение в Листе кода валюты по ее имени

            int type_int = 0;
            string type = "";
            if(radioButton2.Checked == true)
            {
                type_int = 2;
                type = radioButton2.Text;
            }
            else
            {
              type_int = 1;
              type = radioButton1.Text;
            }

            decimal proccent = 0;

            int day = 0;//элемент срок     
            if ((textBox1.Text == "") && (textBox2.Text == "") && (textBox4.Text == "") && (textBox8.Text == "") && (textBox7.Text == ""))
            {
                MessageBox.Show("Все поля обязательны для заполнения!\nДанный вклад не добавлен в список!\nПроверьте все поля, поле Срок max необязателен для ввода");
            }
            else
            {
                name = textBox1.Text;//имя вклада
                day_min = Convert.ToInt32(textBox2.Text);//срок мин
                amount_min = Convert.ToInt32(textBox4.Text);//сумма мин
                description = textBox7.Text;//описание
                currency_for_list = comboBox1.SelectedItem.ToString();//для листа
                proccent = Convert.ToDecimal(textBox8.Text);

                try
                {

                    string strInsertTypeDeposit;
                    if (textBox3.Text == String.Empty)
                    {
                        strInsertTypeDeposit = string.Format("INSERT INTO Types_of_credits_deposits VALUES ('{0}','{1}','{2}','{3}','{4}','{5}', NULL,'{6}','{7}')", id, name, currency, amount_min, proccent, day_min, type_int, description);
                    }

                    else
                    {
                        day_max = Convert.ToInt32(textBox3.Text);
                        day = day_max - day_min;
                        strInsertTypeDeposit = string.Format("INSERT INTO Types_of_credits_deposits VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", id, name, currency, amount_min, proccent, day_min, day_max, type_int, description);


                    }
                    SqlCommand cmdInsertTypeDeposit = new SqlCommand(strInsertTypeDeposit, cn);
                    cmdInsertTypeDeposit.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка: Код вклада должен быть уникальным значением!\n Некорректный ввод данных!\nПроверьте поля: Тип вклада, Валюта\n{0}", ex.Message);
                }

                TypeDeposit type_element = new TypeDeposit(id, name, type, day, currency_for_list, proccent);
            }

            cn.Close();

        }

    }
}
