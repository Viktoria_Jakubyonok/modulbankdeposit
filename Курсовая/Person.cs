﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Курсовая
{
   abstract class Person
    { //закрытые поля
        private int id;
        private string name;
        private string surname;
        private string patronymic;
        private DateTime birthday;
       //так как обработчиков для Person пока нет, то лист создавать не буду
        //конструктор
        public Person(int id, string surname, string name, string patronymic, DateTime birthday)
        {
            this.id = id;
            this.surname = surname;
            this.name = name;
            this.patronymic = patronymic;
            this.birthday = birthday;

        }

        //свойства
        public int Id
        {
            get { return id; }
            set { id = value; }

        }

        public string Name
        {
            get { return name; }
        }

        public string Surname
        {
            get { return surname; }
            set { surname = value; } 
        }

        public string Patronymic
        {
            get { return patronymic; }
        }

        public DateTime BirthDay
        {
            get { return birthday; }
        }

        public int Age()//метод, возвращающий возраст!!!будет вызываться из классов наследниках
   {
       int year = DateTime.Now.Year - BirthDay.Year;
       if (DateTime.Now.Month < BirthDay.Month || (DateTime.Now.Month ==  BirthDay.Month && DateTime.Now.Day < BirthDay.Day))
                    {
                       year--;
                    }
       return year;

   }

        public abstract void Display();//абстрактный метод, который будет выводить информацию в сообщение


    }
}
