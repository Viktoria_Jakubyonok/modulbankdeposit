﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;



namespace Курсовая
{
    public partial class TypeDep : Form
    {
        public AddType adt = new AddType();//переход на форму
        public TypeDep()
        {
            InitializeComponent();

            //Type.TypeDepositList.Clear();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = @"Data Source =(local)\SQLEXPRESS;" +
                "Integrated Security = SSPI;" +
                "Initial Catalog = OOP";
            cn.Open();

            string strSelectType = "Select Loan_Deposits_type_code, Types_of_credits_deposits.Name_type, Type_dep.name_type, Currency.Name, ISNULL((Term_max-Term_min), 0), Rate from Types_of_credits_deposits inner join Type_dep on Type_dep.id_type = Types_of_credits_deposits.Type_id inner join Currency on Currency.Currency_code = Types_of_credits_deposits.Currency_code";
            SqlCommand cmdSelectType = new SqlCommand(strSelectType, cn);
            SqlDataReader typeReader = cmdSelectType.ExecuteReader();

            while (typeReader.Read())
            {
                int id_type = typeReader.GetInt32(0);
                string name = typeReader.GetString(1);
                string typedep = typeReader.GetString(2);
                string currency = typeReader.GetString(3);
                int day = typeReader.GetInt32(4);
                decimal proccent = typeReader.GetDecimal(5);

                TypeDeposit type = new TypeDeposit(id_type, name, typedep, day, currency, proccent);

            }

            cn.Close();

            for (int i = 0; i < TypeDeposit.TypeDepositList.Count; i++)
            {
                string columns_1 = (TypeDeposit.TypeDepositList[i].Name).ToString();//дата открытия
                string columns_2 = (TypeDeposit.TypeDepositList[i].TypeDep).ToString();
                string columns_3 = (TypeDeposit.TypeDepositList[i].Currency).ToString();
                string columns_4;
                if ((TypeDeposit.TypeDepositList[i].Day).ToString() == "0")
                {
                    columns_4 = "неограниченный срок";
                }
                else
                {
                    columns_4 = (TypeDeposit.TypeDepositList[i].Day).ToString();
                }

                string columns_5 = (TypeDeposit.TypeDepositList[i].Proccent).ToString();
                string columns_6 = (TypeDeposit.TypeDepositList[i].ID).ToString();

                dataGridView1.Rows.Add(columns_1, columns_2, columns_3, columns_4, columns_5, columns_6);

            } 

        }

        private void button1_Click(object sender, EventArgs e)
        {
            adt = new AddType();

            adt.ShowDialog();
            adt.Owner = this;
            Close();
        }

        private void TypeDep_Load(object sender, EventArgs e)
        {

         }

        private void button3_Click(object sender, EventArgs e)
        {
           TypeDeposit.SearchType(dataGridView1, textBox1, 5); //поиск вклада по его коду
           textBox1.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TypeDeposit.ProccentSort(); //сортировка по процентной ставке метод дл
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        }
    }

