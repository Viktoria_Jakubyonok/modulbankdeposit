﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Курсовая
{
    class Address
    {
        private string street;
        private string house;
        private City city;

        public Address (string street, string house, City city)
            
        {
            this.street = street;
            this.house = house;
            this.city = city;

        }

        public string Street
        {
            get { return street; }
            set { street = value; }
        }

        public string House
        {
            get { return house; }
            set { house = value; }
        }

        public City City
        {
            get { return city; }
            set { city = value; }
        }
   
    }
}
