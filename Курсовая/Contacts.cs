﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    namespace Курсовая
    {
        class Contacts
        {
            private Address locations;
            private string phone;

            public Contacts (Address locations, string phone)
            {
                this.locations = locations;
                this.phone = phone;

            }

            public Address Address
            {
                get { return locations; }
                set { locations = value; }
            }

            public string Phone
            {
                get { return phone; }
                set { phone = value; }
            }

        }
    }
