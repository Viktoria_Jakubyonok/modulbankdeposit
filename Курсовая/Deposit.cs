﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using System.Drawing;

namespace Курсовая
{
    class Deposit: Account

    {
        
        private decimal addproccent;
        private Client client;
        private DateTime dateoppen;
        private TypeDeposit type;
        public static List<Deposit> DepositList = new List<Deposit>();
        private DateTime dateclose;
        private string status;

        //public Deposit(decimal proccent, int countday, Client client, DateTime dateoppen, decimal balance, string currency, int idacc, Type type, string status)
        //    : base(idacc, balance, currency)
        //{

        //    this.proccent = proccent;
        //    this.countday = countday;
        //    this.client = client;
        //    this.dateoppen = dateoppen;
        //    this.type = type;
        //    this.status = status;
        //    DepositList.Add(this);

        //}

        public Deposit(decimal addproccent, Client client, DateTime dateoppen, DateTime dateclose, decimal balance, int idacc, TypeDeposit type, string status)
            : base(idacc, balance)
        {

            this.addproccent = addproccent;
            this.client = client;
            this.dateoppen = dateoppen;
            this.dateclose = dateclose;
            this.type = type;
            this.status = status;
            DepositList.Add(this);

        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public decimal AddProccent
        {
            get { return Math.Round(addproccent, 3); }
            set { addproccent = value; }
        }


        public Client Client
        {
            get { return client; }
            set { client = value; }
        }

        public DateTime DateOppened
        {
            get { return dateoppen; }
        }

        public DateTime DateClose
        {
            get { return dateclose; }
            set { dateclose = value; }
        }

        public TypeDeposit Type
        {
            get { return type; }
            set { type = value; }
        }

        public static int CountDeposit(string st)//количество закрытых/открытых вкладов в банке
        {
            int p = 0;
            for (int i = 0; i < DepositList.Count; i++)
            {
                if (DepositList[i].Status == st)
                {
                    p++;
                }
            }
            return p;

        }
        public static decimal AmmountProccent()//метод возращает сумму процентов по всем вкладам
        {
           decimal p =0;
            for (int i =0; i<DepositList.Count; i++)
            {
                p = p + DepositList[i].AddProccent;
            }
           return p;
        }

        public static void SearchAccount(DataGridView dt, TextBox textbox, int cells)//метод поиска информации о типах вклада по номеру вклада
        {

            for (int i = 0; i < dt.Rows.Count; i++) //при повторном поиске будет окрашиваться сначала вся сетка в прежний цвет
            {
                dt.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }

            string id_name = "";
           // string type = "";


            for (int i = 0; i < DepositList.Count; i++) //организация поиска по спиcку
            {
                if (DepositList[i].Idacc.ToString() == textbox.Text)
                {
                    id_name = DepositList[i].Idacc.ToString();
                    string stringMessageBox = "Фамилия: " + DepositList[i].Client.Surname + "\nИмя: " + DepositList[i].Client.Name + "\nОтчество: " + DepositList[i].Client.Patronymic + "\nПаспортный данные: " + DepositList[i].Client.Passport + "\nДата рождения: " + DepositList[i].Client.BirthDay + "\nМоб. телефон: " + DepositList[i].Client.Contacts.Phone + "\nТип вклада: " + DepositList[i].Type.TypeDep + "\nВалюта вклада: " + DepositList[i].Type.Currency + "\nНаименование вклада: " + DepositList[i].Type.Name + "\nПроцентная ставка: " + DepositList[i].Type.Proccent + "\nНачисленние процентов: " + DepositList[i].AddProccent + "\nДата открытия: " + DepositList[i].DateOppened + "\nСтатус: " + DepositList[i].Status;
                    MessageBox.Show(stringMessageBox);
                }
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string l = Convert.ToString(dt.Rows[i].Cells[cells].Value);
                if (id_name == l)
                {
                    dt.Rows[i].DefaultCellStyle.BackColor = Color.Aqua;
                }

            }

        }

        public static void ComboboxAddFromBase(SqlDataReader reader,  ArrayList list, ComboBox combobox_name) //метод загрузки значений из базы данных в comboBox
        {
            string name;
            int id;
            while (reader.Read())
            {
                name = reader.GetString(0);
                id = reader.GetInt32(1);
                list.Add(name);//на четных позиция наименование 
                list.Add(id);//на нечетных позициях код
            }

            combobox_name.Items.Clear();
            for (int i = 0; i < list.Count; i = i + 2)
            {

                combobox_name.Items.Add(list[i]);
            }
        }

        public static int SearchCodefromCombobox (ArrayList list, ComboBox comboBox)//возращает соответсвующее значение элемента в коде в БД выбранного в comboBox
        {
            string element_name = comboBox.SelectedIndex.ToString();//индекс в комбобокс
            int element = 0;//код в комбобокс

            for (int i = 0; i < list.Count; i++) //для нахождение в Листе кода должности по ее имени
            {
                if (i / 2 == Convert.ToInt32(element_name))
                {
                    element = Convert.ToInt32(list[i + 1]);
                    break;
                }
            }

            return element;
        }

        public void SimpleProccent()//расчет простых процентов 
        {
            if (status == "активен   ") //если вклад еще действителен
            {
                //time = DateTime.Now - DateOppened;
                //int day = time.Days;

                AddProccent = (Balance * type.Proccent * Day()) / (365 * 100);//проценты по вкладу
            }
            else //если срок вклада закончился
            {
                AddProccent = (Balance * type.Proccent * type.Day) / (365 * 100);
                //Balance = ((Balance * type.Proccent * type.Day) / (365 * 100)) + Balance;
            }

        }

        public void StatusChange() //если срок вклада вышел, то изменяется его статус
        {
            if (type.TypeDep == "срочный             ")
            {
                if (DateClose < DateTime.Now)
                {
                    Status = "пассивен  ";
                }
            }
        }

        public void ComplexProccent()//расчет сложных процентов
        {
            double stepen; //если вклад открыт
            if (Status == "активен   ")
            {
                stepen = ((DateTime.Now.Year - DateOppened.Year) * 12) + DateTime.Now.Month - DateOppened.Month;
                AddProccent = Convert.ToDecimal(Math.Pow(Convert.ToDouble(1 + ((type.Proccent * 30) / (100 * 365))), stepen));
                //Balance = (Balance * Convert.ToDecimal(AddProccent)) + Balance;
            }
           //если вклад закрыт, не имеет смысла проводить повторные вычисления
        }

        public int Day()//возращает количество дней существования вклада
        {
            TimeSpan time;
            if (Type.TypeDep == "срочный             ")
            {
                if (Status == "активен   ")
                {
                    time = DateTime.Now - DateOppened;
                    return time.Days;
                }
                else
                {
                    return Type.Day;
                }
            }

            else
            {
                if (Status == "активен   ")
                {
                    time = DateTime.Now - DateOppened;
                    return time.Days;
                }
                else
                {
                    time = DateClose - DateOppened;
                    return time.Days;
                }
            }
        }

        public decimal Result()//вывод суммы с учетом процентов
        {
            decimal result = AddProccent + Balance;
            return result;
        }


        public override string Info()//не задействован, для наглядности
        {
            // Используется ссылка на метод, определенный в базовом классе Account
            return base.Info() + "Проценты: " + AddProccent + "\n";
        }
    }
}
