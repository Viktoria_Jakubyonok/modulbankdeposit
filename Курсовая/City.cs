﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Курсовая
{
    class City
    { 
        private string region;
        private string town;
        public static List<City> CityList = new List<City>();

        //конструктор
        public City( string region, string town)
        {

            this.region = region;
            this.town = town;
            CityList.Add(this);
            
        }

        public string Region
        {
            get { return region; }
        }

        public string Town
        {
            get { return town; }
            set { town = value; }
        }

      
    }
}
