USE [master]
GO
/****** Object:  Database [OOP]    Script Date: 13.12.2018 11:48:09 ******/
CREATE DATABASE [OOP]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'OOP', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\OOP.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'OOP_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\OOP_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [OOP] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OOP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [OOP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [OOP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [OOP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [OOP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [OOP] SET ARITHABORT OFF 
GO
ALTER DATABASE [OOP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [OOP] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [OOP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [OOP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [OOP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [OOP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [OOP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [OOP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [OOP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [OOP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [OOP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [OOP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [OOP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [OOP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [OOP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [OOP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [OOP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [OOP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [OOP] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [OOP] SET  MULTI_USER 
GO
ALTER DATABASE [OOP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [OOP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [OOP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [OOP] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [OOP]
GO
/****** Object:  StoredProcedure [dbo].[AverageEmployeesSort]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[AverageEmployeesSort]
as
Select Surname, Name, Patronymic, Job_name, Department_name, Average_annual_output from [Employee's_position] 
inner join Employees on Employees.Job_code = [Employee's_position].Job_code 
inner join Divisions on [Employee's_position].Department_code = Divisions.Department_code
order by Average_annual_output desc

GO
/****** Object:  StoredProcedure [dbo].[CountTime]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountTime] @Employee_code as int
as
select Surname, Name, Patronymic, SUM([The_time _of_the]) as 'время работы' from Currency_exchange
inner join Employees 
on Currency_exchange.Employee_code = Employees.Employee_code
where Currency_exchange.Employee_code= @Employee_code
group by Surname, Name, Patronymic

GO
/****** Object:  StoredProcedure [dbo].[CreditTime]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CreditTime] @Operation_code as int, @time as int
as
select Surname, Name, Patronymic, Name_type, [The_time _of_the] from Credits_Deposits
inner join Employees on Credits_Deposits.Employee_code = Employees.Employee_code
inner join Types_of_credits_deposits on Credits_Deposits.Loan_Deposits_type_code=Types_of_credits_deposits.Loan_Deposits_type_code
where Operation_code =@Operation_code AND [The_time _of_the] < @time
ORDER BY  [The_time _of_the]

GO
/****** Object:  StoredProcedure [dbo].[CurrencyEx]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CurrencyEx]
as Select Surname, Employees.Name, Patronymic, Currency.Name, Operation_name, sum(Currency_exchange.Amount_of_currency_purchased) as 'принято', sum( Currency_exchange.Amount_of_currency_sold) 'обменено' from ((Currency_exchange inner join Operations on Currency_exchange.Operation_code = Operations.Operation_code) 
inner join Employees on Employees.Employee_code = Currency_exchange.Employee_code) inner join Currency on Currency.Currency_code = Currency_exchange.Currency_code 
Group by  Currency.Name, Operation_name, Surname, Employees.Name, Patronymic
order by Operation_name

GO
/****** Object:  StoredProcedure [dbo].[CurrencyTime]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CurrencyTime] as
SELECT Surname, Name, Patronymic, Operation_name, AVG ([The_time _of_the]) AS 'среднее время на выполнение',
sum ([The_time _of_the]) AS 'Потраченное время', count([The_time _of_the]) as 'количество операций'
from Employees inner join Currency_exchange ON Currency_exchange.Employee_code=Employees.Employee_code inner join Operations ON Operations.Operation_code=Currency_exchange.Operation_code
GROUP BY SURNAME, NAME, Patronymic, Operation_name

GO
/****** Object:  StoredProcedure [dbo].[EmployeeOperation]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EmployeeOperation]
as
Select (select count(Credit_Deposit_number) from Credits_Deposits)+
(select count(Operation_number) from Currency_exchange)+
(select count(Operation_number) from Repayment_receipt) as 'количество операций выполненных банком',
 (Select count(Employee_code)  from Employees 
inner join [Employee's_position]
on Employees.Job_code = [Employee's_position].Job_code
where [Employee's_position].Department_code between 1 and 3) as 'количество сотрудников',
(Select (select count(Credit_Deposit_number) from Credits_Deposits)+
(select count(Operation_number) from Currency_exchange)+
(select count(Operation_number) from Repayment_receipt))
/(Select count(Employee_code)  from Employees 
inner join [Employee's_position]
on Employees.Job_code = [Employee's_position].Job_code
where [Employee's_position].Department_code between 1 and 3)
as  'количество операций, в среднем приходящихся на 1 сотрудника'

GO
/****** Object:  StoredProcedure [dbo].[IncomeCredit]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[IncomeCredit]
as
select Surname, Name, Patronymic, sum([Bank_rate]) as 'прибыль банка', count([The_time _of_the]) as  'количество операций'
from Employees inner join Credits_Deposits on Employees.Employee_code=Credits_Deposits.Employee_code
where Operation_code = 3
GROUP BY Surname, NAME, Patronymic

GO
/****** Object:  Table [dbo].[City]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[City](
	[City_code] [int] IDENTITY(1,1) NOT NULL,
	[Region_code] [int] NOT NULL,
	[City_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[City_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clients](
	[Client_code] [int] NOT NULL,
	[Surname] [varchar](15) NOT NULL,
	[Name] [varchar](15) NOT NULL,
	[Patronymic] [varchar](15) NOT NULL,
	[Date_of_Birth] [smalldatetime] NOT NULL,
	[Passport_series] [varchar](9) NOT NULL,
	[City] [int] NOT NULL,
	[Street] [varchar](50) NOT NULL,
	[House] [varchar](5) NOT NULL,
	[Phone_number] [nchar](11) NOT NULL,
	[Email] [nchar](40) NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Client_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Credits_Deposits]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Credits_Deposits](
	[Credit_Deposit_number] [int] NOT NULL,
	[Loan_Deposits_type_code] [int] NOT NULL,
	[Client_code] [int] NOT NULL,
	[Employee_code] [int] NOT NULL,
	[Amount] [money] NOT NULL,
	[Date_of_issue_opening] [datetime] NOT NULL,
	[Date_of_redemption_closing] [datetime] NULL,
	[Bank_rate] [money] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_Credits] PRIMARY KEY CLUSTERED 
(
	[Credit_Deposit_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[Currency_code] [int] NOT NULL,
	[Name] [nchar](20) NOT NULL,
	[Сountry] [nchar](105) NULL,
	[Description] [nchar](50) NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[Currency_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency_exchange]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency_exchange](
	[Operation_number] [int] NOT NULL,
	[Currency_code] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Course] [smallmoney] NOT NULL,
	[Employee_code] [int] NOT NULL,
	[Amount_of_currency_sold] [smallmoney] NOT NULL,
	[Amount_of_currency_purchased] [smallmoney] NOT NULL,
	[Operation_code] [int] NOT NULL,
	[Client_code] [int] NULL,
	[The_time _of_the] [float] NOT NULL,
 CONSTRAINT [PK_Currency_exchange_1] PRIMARY KEY CLUSTERED 
(
	[Operation_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Divisions]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Divisions](
	[Department_code] [int] IDENTITY(1,1) NOT NULL,
	[Department_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Divisions] PRIMARY KEY CLUSTERED 
(
	[Department_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[Employee_code] [int] NOT NULL,
	[Surname] [varchar](15) NOT NULL,
	[Name] [varchar](15) NOT NULL,
	[Patronymic] [varchar](15) NOT NULL,
	[Phone_number] [nchar](11) NULL,
	[Date_of_Birth] [datetime] NOT NULL,
	[Job_code] [int] NOT NULL,
	[Qualification] [nchar](30) NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[Employee_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employee's_position]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee's_position](
	[Job_code] [int] NOT NULL,
	[Department_code] [int] NOT NULL,
	[Job_name] [varchar](50) NOT NULL,
	[Working_day] [int] NULL,
 CONSTRAINT [PK_Employee's_position] PRIMARY KEY CLUSTERED 
(
	[Job_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Log]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[int_operation] [int] IDENTITY(1,1) NOT NULL,
	[idacc] [int] NULL,
	[operation_name] [int] NULL,
	[amount_change] [decimal](18, 0) NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[int_operation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Operations]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Operations](
	[Operation_code] [int] IDENTITY(1,1) NOT NULL,
	[Department_code] [int] NOT NULL,
	[Operation_name] [varchar](30) NOT NULL,
	[Discription] [varchar](50) NULL,
 CONSTRAINT [PK_Operations] PRIMARY KEY CLUSTERED 
(
	[Operation_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Region]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Region](
	[Region_code] [int] IDENTITY(1,1) NOT NULL,
	[Region_name] [varchar](50) NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Region_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[id_status] [int] NOT NULL,
	[name] [char](10) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[id_status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Type_dep]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type_dep](
	[id_type] [int] NOT NULL,
	[name_type] [nchar](20) NOT NULL,
 CONSTRAINT [PK_Type_dep] PRIMARY KEY CLUSTERED 
(
	[id_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Types_of_credits_deposits]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Types_of_credits_deposits](
	[Loan_Deposits_type_code] [int] NOT NULL,
	[Name_type] [nchar](100) NOT NULL,
	[Currency_code] [int] NOT NULL,
	[Amount_min] [money] NOT NULL,
	[Rate] [decimal](7, 2) NOT NULL,
	[Term_min] [int] NOT NULL,
	[Term_max] [int] NULL,
	[Type_id] [int] NULL,
	[Description] [nchar](100) NULL,
 CONSTRAINT [PK_Types_of_loans] PRIMARY KEY CLUSTERED 
(
	[Loan_Deposits_type_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Credit_Employees_7]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Credit_Employees_7] as
select Loan_Deposits_type_code, Date_of_issue_opening, Bank_rate from Credits_Deposits 
inner join Employees on Credits_Deposits.Employee_code= Employees.Employee_code 
where Credits_Deposits.Employee_code =7

GO
/****** Object:  View [dbo].[Credit_more_5000]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[Credit_more_5000] as
Select Surname, Name, Patronymic, Credit_Deposit_number, Date_of_issue_opening, Amount from Employees 
inner join Credits_Deposits ON Credits_Deposits.Employee_code = Employees.Employee_code 
where Credits_Deposits.Amount > 5000;

GO
/****** Object:  View [dbo].[EmplCred30min]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[EmplCred30min] as
select Surname, Name, Patronymic, Name_type, [The_time _of_the] from Credits_Deposits
inner join Employees on Credits_Deposits.Employee_code = Employees.Employee_code
inner join Types_of_credits_deposits on Credits_Deposits.Loan_Deposits_type_code=Types_of_credits_deposits.Loan_Deposits_type_code
where Credit_Deposit_number like '1%' AND [The_time _of_the] < 30

GO
/****** Object:  View [dbo].[EmplDeposit]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[EmplDeposit] as
select Surname, Name, Patronymic, Credit_Deposit_number, Name_type, [The_time _of_the] from Credits_Deposits
inner join Employees on Credits_Deposits.Employee_code = Employees.Employee_code
inner join Types_of_credits_deposits on Credits_Deposits.Loan_Deposits_type_code=Types_of_credits_deposits.Loan_Deposits_type_code
where Credit_Deposit_number like '2%'

GO
/****** Object:  View [dbo].[HigherEducation_CurExDep]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[HigherEducation_CurExDep] as
Select Surname, Name, Patronymic, Average_annual_output, Number_of_days_worked from [Employee's_position] 
inner join Employees on Employees.Job_code = [Employee's_position].Job_code 
where (Qualification = 'высшее' and Department_code = 1)

GO
/****** Object:  View [dbo].[RepaymReceipt]    Script Date: 13.12.2018 11:48:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[RepaymReceipt] as
select Surname, Name, Patronymic,
Name_type, Date_of_issue_opening, Date, Credits_Deposits.Amount as 'первоначальная сумма', Repayment_receipt.Amount as 'cумма к выдаче'
from Repayment_receipt
inner join Employees on Repayment_receipt.Employee_code=Employees.Employee_code
inner join Credits_Deposits on Credits_Deposits.Credit_Deposit_number = Repayment_receipt.Credit_Depozit_number
inner join Types_of_credits_deposits on  Credits_Deposits.Loan_Deposits_type_code=Types_of_credits_deposits.Loan_Deposits_type_code
where Repayment_receipt.Operation_code = 6

GO
SET IDENTITY_INSERT [dbo].[City] ON 

INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (1, 1, N'Пинск')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (3, 1, N'Брест')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (4, 1, N'Барановичи')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (5, 1, N'Кобрин')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (6, 1, N'Лунинец')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (7, 1, N'Пружаны')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (8, 1, N'Дрогичин')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (9, 1, N'Ганцевичи')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (10, 1, N'Жабинка')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (12, 1, N'Столин')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (13, 2, N'Витебск')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (14, 2, N'Летцы')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (15, 2, N'Миоры')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (16, 2, N'Полоцк')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (17, 2, N'Новополоцк')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (18, 3, N'Житковичи')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (19, 3, N'Мозырь')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (20, 3, N'Гомель')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (21, 3, N'Речица')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (22, 4, N'Ружаны')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (23, 4, N'Волковыск')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (24, 5, N'Могилев')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (25, 6, N'Минск')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (26, 7, N'Клецк')
INSERT [dbo].[City] ([City_code], [Region_code], [City_name]) VALUES (27, 7, N'Любань')
SET IDENTITY_INSERT [dbo].[City] OFF
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (1, N'Пучинский', N'Евгений', N'Викторович', CAST(0x8DF30000 AS SmallDateTime), N'AB1496587', 1, N'Пролетарская', N'117-2', N'80295675595', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (2, N'Рапейко', N'Дмитрий', N'Владимирович', CAST(0x8E220000 AS SmallDateTime), N'АВ1247896', 1, N'Заводская', N'224', N'80299880692', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (3, N'Шиманчик', N'Ирина', N'Валентиновна', CAST(0x8DF50000 AS SmallDateTime), N'АВ1475628', 5, N'Московская', N'25-6', N'80297180751', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (4, N'Шевчук', N'Дмитрий', N'Сергеевич', CAST(0x8BFC0000 AS SmallDateTime), N'АВ2561478', 6, N'Телефонная', N'21-25', N'80447312588', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (5, N'Шарко', N'Алексей', N'Анатольевич', CAST(0x8D910000 AS SmallDateTime), N'АВ4571236', 4, N'Шевченко', N'5-3', N'80299133919', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (6, N'Андриенко', N'Артём', N'Андреевич', CAST(0x8D520000 AS SmallDateTime), N'АВ4578326', 7, N'Дальняя', N'78', N'80299987351', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (7, N'Рафалович', N'Степан', N'Леонидович', CAST(0x8E0A0000 AS SmallDateTime), N'АВ6528947', 9, N'Калинина', N'23', N'80292845515', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (8, N'Шершуков', N'Виктор ', N'Геннадьевич', CAST(0x79C90000 AS SmallDateTime), N'МС2587463', 25, N'Ленина', N'25-6', N'80294783652', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (9, N'Лучников', N'Виктор', N'Кузьмич', CAST(0x6BD60000 AS SmallDateTime), N'AB2571469', 1, N'Соболева', N'24', N'80293752146', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (10, N'Битова', N'Анастасия', N'Юрьевна', CAST(0x74A60000 AS SmallDateTime), N'АВ1476325', 1, N'Гончарная', N'28-78', N'80336512641', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (11, N'Кириллов', N'Валентин', N'Владиславович', CAST(0x69C70000 AS SmallDateTime), N'ВМ2413698', 13, N'Пролетарская', N'23-6', N'80447037430', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (12, N'Игнатьев', N'Игорь', N'Дмитриевич', CAST(0x81AD0000 AS SmallDateTime), N'КВ1456289', 24, N'Василькова', N'2-1', N'80294512368', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (13, N'Самохвалова', N'Наталия', N'Алексеевна', CAST(0x63FE0000 AS SmallDateTime), N'АВ2147896', 3, N'Любаньская', N'2', N'80332567412', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (14, N'Павлова', N'Лариса', N'Максимовна', CAST(0x755C0000 AS SmallDateTime), N'АВ1238596', 3, N'Кулешова', N'15', N'80441257463', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (15, N'Баршев', N'Андрей', N'Николаевич', CAST(0x67710000 AS SmallDateTime), N'HB1874362', 18, N'Комсомольская', N'23-95', N'80251785272', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (16, N'Оверченков', N'Захар', N'Михайлович', CAST(0x63540000 AS SmallDateTime), N'АВ4765289', 6, N'Репина', N'21', N'80296513429', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (17, N'Бодалян', N'Оксана', N'Геннадиевна', CAST(0x5B460000 AS SmallDateTime), N'НВ1298567', 19, N'Кособьяна', N'36', N'80296576961', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (18, N'Герасимов', N'Александр', N'Платонович', CAST(0x6A9E0000 AS SmallDateTime), N'МР1295634', 25, N'Желтая', N'14-7', N'80291785272', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (19, N'Леденева', N'Софья', N'Ильинична', CAST(0x722B0000 AS SmallDateTime), N'НВ4765321', 20, N'Мира', N'8', N'80332106520', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (20, N'Ватолкина', N'Светлана', N'Эдуардовна', CAST(0x7B660000 AS SmallDateTime), N'НВ9845273', 21, N'Белова', N'16-4', N'80446987412', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (21, N'Волик', N'Борис', N'Степанович', CAST(0x71830000 AS SmallDateTime), N'АВ1485367', 5, N'Пролетарская', N'26', N'80293751982', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (22, N'Архипов', N'Яков', N'Семенович', CAST(0x6BD90000 AS SmallDateTime), N'КН1937256', 22, N'Дыдышко', N'25', N'80336134598', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (23, N'Гусева', N'Екатерина', N'Павловна', CAST(0x7B4B0000 AS SmallDateTime), N'ВМ1596874', 14, N'Гоголя', N'13', N'80336576162', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (24, N'Чапко', N'Полина', N'Артемовна', CAST(0x70350000 AS SmallDateTime), N'ВМ1476925', 15, N'Куйбышова', N'17-6', N'80291745236', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (25, N'Кириллов', N'Наум', N'Давидович', CAST(0x6D6F0000 AS SmallDateTime), N'КВ1698574', 26, N'Воронина', N'23-21', N'80336478521', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (26, N'Рощина', N'Калерия', N'Эдуардовна', CAST(0x6AD00000 AS SmallDateTime), N'КН4786523', 23, N'Русская', N'12', N'80297416985', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (27, N'Ильин', N'Максим', N'Альбертович', CAST(0x7CE20000 AS SmallDateTime), N'МР1463257', 25, N'Победы', N'28', N'80291476325', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (28, N'Комарова', N'Галина', N'Антоновна', CAST(0x6AE80000 AS SmallDateTime), N'ВМ1968547', 16, N'Соболевская', N'56', N'80335981463', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (29, N'Ефимов', N'Денис', N'Александрович', CAST(0x7CA50000 AS SmallDateTime), N'ВМ1496357', 17, N'Рокоссовского', N'52-3', N'80294536278', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (30, N'Диденко ', N'Роза', N' Фёдоровна', CAST(0x70490000 AS SmallDateTime), N'МС5687283', 27, N'Дудукина', N'7', N'80332416398', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (31, N'Ященко', N'Игорь', N'Фёдорович', CAST(0x8ED60000 AS SmallDateTime), N'BM1234567', 4, N'Дыдышко', N'25', N'80296576961', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (32, N'Калуго', N'Анна', N'Анатольевна', CAST(0x8D690000 AS SmallDateTime), N'BM1235784', 15, N'ИПД', N'5', N'80296576961', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (33, N'Петриченко', N'Олег ', N'Викторович', CAST(0x9E0B0000 AS SmallDateTime), N'ВМ1254125', 6, N'ИПД', N'54', N'80255323441', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (34, N'Ященко', N'Роман', N'Петрович', CAST(0x8D480000 AS SmallDateTime), N'ВМ1236574', 5, N'Баграмяна', N'25', N'80254536214', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (35, N'Валюха', N'Алина', N'Аркадьевна', CAST(0x8ED60000 AS SmallDateTime), N'ВМ2541639', 5, N'Баграмяна', N'д', N'80296576961', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (36, N'Тетерев', N'Олег', N'Александрович', CAST(0x8ED60000 AS SmallDateTime), N'ВМ4512389', 21, N'Ипд', N'5', N'80296576961', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (37, N'Великан', N'Екатерина', N'Ивановна', CAST(0xA77E0000 AS SmallDateTime), N'DV1456237', 17, N'Володарского', N'5', N'80296576961', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (38, N'Вуж', N'Жанна', N'Аркадьевна', CAST(0x93200000 AS SmallDateTime), N'DF1234567', 4, N'Гоголя', N'2', N'80254123658', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (39, N'Лещенко', N'Кирилл', N'Иванович', CAST(0x931E0000 AS SmallDateTime), N'NM1478523', 7, N'Гоголя', N'5', N'147855695  ', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (40, N'Деденко', N'Петр', N'Александрович', CAST(0x8D690000 AS SmallDateTime), N'DV1256934', 5, N'Мира', N'2', N'20134569874', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (41, N'Цуба', N'Михаил', N'Викторивич', CAST(0x8DC20000 AS SmallDateTime), N'DV1463258', 7, N'Весны', N'5', N'80293673652', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (42, N'Петров', N'Артем', N'Игнатьевич', CAST(0x8ED60000 AS SmallDateTime), N'DH1254789', 7, N'Мира', N'2', N'80253652148', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (43, N'Вил', N'Виктория', N'Олеговна', CAST(0x8F300000 AS SmallDateTime), N'GH1423658', 9, N'Красного октября', N'5', N'80291543654', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (44, N'Цыпух', N'Ольга', N'Викторовна', CAST(0x8F4F0000 AS SmallDateTime), N'df8523697', 5, N'Великая', N'5', N'20532145698', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (45, N'Джалов', N'Дмитрий', N'Михайлович', CAST(0x8F4F0000 AS SmallDateTime), N'RT8523697', 5, N'Великая', N'5', N'20532145698', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (46, N'Ликун', N'Павел', N'Павлович', CAST(0x8F4F0000 AS SmallDateTime), N'DV4785691', 7, N'Гоголя', N'1', N'80253651458', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (47, N'Жук', N'Илья', N'Викторович', CAST(0x8ED60000 AS SmallDateTime), N'GH4786523', 6, N'ИПД', N'8', N'80253651456', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (48, N'Шумаш', N'Елена', N'Петровна', CAST(0x8ED60000 AS SmallDateTime), N'DV4521465', 6, N'ИПД', N'5', N'80295631458', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Clients] ([Client_code], [Surname], [Name], [Patronymic], [Date_of_Birth], [Passport_series], [City], [Street], [House], [Phone_number], [Email]) VALUES (49, N'Куй', N'Жоззе', N'Иванович', CAST(0x8ED60000 AS SmallDateTime), N'DG4521365', 9, N'Баграмяна', N'8', N'80254789635', N'viktoria.jakubyonok@gmail.com           ')
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000001, 2014, 1, 8, 1000.0000, CAST(0x0000A7E5008F18D0 AS DateTime), CAST(0x0000A89A00000000 AS DateTime), 15.7810, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000002, 2018, 3, 8, 5000.0000, CAST(0x0000A7E5009A5BA0 AS DateTime), CAST(0x0000A95200000000 AS DateTime), 239.1780, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000003, 2020, 5, 8, 10000.0000, CAST(0x0000A7E500A1C610 AS DateTime), CAST(0x0000A95200000000 AS DateTime), 0.0000, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000004, 2021, 7, 8, 2000.0000, CAST(0x0000A7E500ABA950 AS DateTime), CAST(0x0000A7E500000000 AS DateTime), 110.9590, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000005, 2025, 9, 8, 500.0000, CAST(0x0000A7E500B28720 AS DateTime), CAST(0x0000A84000000000 AS DateTime), 11.5070, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000006, 2026, 11, 15, 1000.0000, CAST(0x0000A7E6009F9390 AS DateTime), CAST(0x0000A95300000000 AS DateTime), 67.8080, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000007, 2030, 12, 15, 20000.0000, CAST(0x0000A7E600ABA950 AS DateTime), CAST(0x0000A95300000000 AS DateTime), 1139.7260, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000009, 2014, 7, 7, 3000.0000, CAST(0x0000A8AC00F6717C AS DateTime), CAST(0x0000AAD100000000 AS DateTime), 69.4360, 1)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000010, 2027, 32, 8, 25000.0000, CAST(0x0000A8EC014B7AFC AS DateTime), CAST(0x0000B08A014B7AFC AS DateTime), 205.4790, 1)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000011, 2014, 33, 8, 500.0000, CAST(0x0000A70E00000000 AS DateTime), CAST(0x0000A9A0014E518C AS DateTime), 7.8900, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000012, 2015, 3, 8, 10000.0000, CAST(0x0000A6EE00000000 AS DateTime), CAST(0x0000A8EC00000000 AS DateTime), 103.5620, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000013, 2040, 35, 8, 12500.0000, CAST(0x00008FC700000000 AS DateTime), NULL, 1.0180, 1)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000014, 2012, 36, 8, 12456.0000, CAST(0x0000A5FD00000000 AS DateTime), CAST(0x0000AB5400000000 AS DateTime), 42.9990, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000016, 2012, 38, 8, 250000.0000, CAST(0x0000A8ED013DCB14 AS DateTime), CAST(0x0000A9A1013DCB14 AS DateTime), 863.0140, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000017, 2002, 39, 8, 50000.0000, CAST(0x0000A8EE000D2F00 AS DateTime), CAST(0x0000AB46000D2F00 AS DateTime), 708.7670, 1)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000022, 2019, 42, 8, 152000.0000, CAST(0x0000A8EE00D36FBC AS DateTime), CAST(0x0000A90D00D36FBC AS DateTime), 0.0000, 2)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000023, 2008, 43, 8, 5000.0000, CAST(0x0000A8EE00D4D8AC AS DateTime), CAST(0x0000AAAB00D4D8AC AS DateTime), 43.3970, 1)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000024, 2041, 44, 8, 120000.0000, CAST(0x0000A8EE00D55034 AS DateTime), NULL, 1.0030, 1)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000025, 2041, 45, 8, 120000.0000, CAST(0x0000A8EE00D58ACC AS DateTime), NULL, 1.0030, 1)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000027, 2025, 47, 8, 50000.0000, CAST(0x0000A8EE00DAFFAC AS DateTime), CAST(0x0000A9DE00DAFFAC AS DateTime), 949.3150, 1)
INSERT [dbo].[Credits_Deposits] ([Credit_Deposit_number], [Loan_Deposits_type_code], [Client_code], [Employee_code], [Amount], [Date_of_issue_opening], [Date_of_redemption_closing], [Bank_rate], [status]) VALUES (2000029, 2008, 49, 8, 15000.0000, CAST(0x0000A8EE00DE759C AS DateTime), CAST(0x0000AAAB00DE759C AS DateTime), 130.1920, 1)
INSERT [dbo].[Currency] ([Currency_code], [Name], [Сountry], [Description]) VALUES (643, N'RUB                 ', N'Россия                                                                                                   ', N'Российский рубль                                  ')
INSERT [dbo].[Currency] ([Currency_code], [Name], [Сountry], [Description]) VALUES (840, N'USD                 ', N'Брит.терр. в Инд.ок.,  Виргинск. о-ва, Гуам, Палау, Панама,  Пуэрто-Рико, США                            ', N'Доллар США                                        ')
INSERT [dbo].[Currency] ([Currency_code], [Name], [Сountry], [Description]) VALUES (933, N'BYN                 ', N'Беларусь                                                                                                 ', N'Белорусский рубль                                 ')
INSERT [dbo].[Currency] ([Currency_code], [Name], [Сountry], [Description]) VALUES (978, N'EUR                 ', NULL, N'Евро                                              ')
INSERT [dbo].[Currency] ([Currency_code], [Name], [Сountry], [Description]) VALUES (980, N'UAH                 ', N'Украина                                                                                                  ', N'Гривна                                            ')
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (1, 840, CAST(0x0000A8AC0087643C AS DateTime), 1.9910, 5, 100.0000, 1991.0000, 2, NULL, 6)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (2, 840, CAST(0x0000A8AC0089506C AS DateTime), 1.9910, 5, 520.0000, 1035.3200, 2, NULL, 7)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (3, 840, CAST(0x0000A8AC008BC93C AS DateTime), 1.9980, 5, 1000.0000, 500.5000, 1, NULL, 4)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (4, 840, CAST(0x0000A8AC008D6F1C AS DateTime), 1.9980, 5, 200.0000, 100.1000, 1, NULL, 9)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (5, 643, CAST(0x0000A8AC00918DCC AS DateTime), 0.0343, 5, 3000.0000, 102.7500, 2, NULL, 10)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (6, 643, CAST(0x0000A8AC009379FC AS DateTime), 0.0343, 5, 50000.0000, 1715.0000, 2, 5, 5)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (7, 643, CAST(0x0000A8AC0095F2CC AS DateTime), 0.0345, 5, 550.0000, 15942.0000, 1, NULL, 7)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (8, 643, CAST(0x0000A8AC0098B1EC AS DateTime), 0.0345, 5, 1000.0000, 28985.5100, 1, 17, 8)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (9, 978, CAST(0x0000A8AC009B710C AS DateTime), 2.4410, 5, 100.0000, 244.1000, 2, NULL, 3)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (10, 978, CAST(0x0000A8AC009E302C AS DateTime), 2.4410, 5, 250.0000, 610.2500, 2, NULL, 4)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (11, 978, CAST(0x0000A8AC00A0EF4C AS DateTime), 2.4540, 5, 300.0000, 122.2500, 1, NULL, 6)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (12, 978, CAST(0x0000A8AC00A3AE6C AS DateTime), 2.4540, 5, 500.0000, 203.7500, 1, NULL, 2)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (13, 980, CAST(0x0000A8AC00A66D8C AS DateTime), 0.0700, 5, 1000.0000, 70.0000, 2, 29, 3)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (14, 980, CAST(0x0000A8AC00A92CAC AS DateTime), 0.0700, 5, 5000.0000, 350.0000, 2, 24, 5)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (15, 980, CAST(0x0000A8AC00ABEBCC AS DateTime), 0.0587, 5, 100.0000, 5.8700, 1, NULL, 1)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (16, 980, CAST(0x0000A8AC00B00A7C AS DateTime), 0.0587, 5, 250.0000, 14.6750, 1, NULL, 2)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (17, 840, CAST(0x0000A8AC00B847DC AS DateTime), 1.9910, 6, 120.0000, 238.9200, 2, NULL, 3)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (18, 840, CAST(0x0000A8AC00B9EDBC AS DateTime), 1.9980, 6, 150.0000, 75.0000, 1, NULL, 4)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (19, 643, CAST(0x0000A8AC00BBD9EC AS DateTime), 0.0343, 6, 250.0000, 8.5750, 2, NULL, 6)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (20, 643, CAST(0x0000A8AC00BD7FCC AS DateTime), 0.0345, 6, 2000.0000, 57971.0000, 1, 2, 2)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (21, 978, CAST(0x0000A8AC00BF25AC AS DateTime), 2.4410, 6, 1000.0000, 2441.0000, 2, 7, 5)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (22, 978, CAST(0x0000A8AC00C22B1C AS DateTime), 2.4540, 6, 200.0000, 81.5000, 1, NULL, 4)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (23, 840, CAST(0x0000A8AC00C7EFAC AS DateTime), 1.9910, 18, 1200.0000, 2389.2000, 2, 18, 7)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (24, 840, CAST(0x0000A8AC00CAAECC AS DateTime), 1.9910, 18, 200.0000, 398.2000, 2, NULL, 3)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (25, 840, CAST(0x0000A8AC00CC9AFC AS DateTime), 1.9910, 18, 2500.0000, 4997.5000, 2, 14, 6)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (26, 840, CAST(0x0000A8AC00CFA06C AS DateTime), 1.9910, 18, 1000.0000, 1991.0000, 2, 12, 5)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (27, 978, CAST(0x0000A8AC00D1D2EC AS DateTime), 2.4410, 18, 1000.0000, 2441.0000, 2, 11, 4)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (28, 978, CAST(0x0000A8AC00D5AB4C AS DateTime), 2.4410, 18, 200.0000, 488.2000, 2, NULL, 3)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (29, 978, CAST(0x0000A8AC00DA569C AS DateTime), 2.4410, 18, 2600.0000, 634.6600, 2, 1, 2)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (30, 978, CAST(0x0000A8AC00E01B2C AS DateTime), 2.4410, 18, 5000.0000, 1220.5000, 2, 5, 6)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (31, 978, CAST(0x0000A8AC0107121B AS DateTime), 2.4400, 18, 5000.0000, 1245.0000, 1, 4, 7)
INSERT [dbo].[Currency_exchange] ([Operation_number], [Currency_code], [Date], [Course], [Employee_code], [Amount_of_currency_sold], [Amount_of_currency_purchased], [Operation_code], [Client_code], [The_time _of_the]) VALUES (32, 978, CAST(0x0000A8AD00DF20D4 AS DateTime), 2.4410, 18, 5000.0000, 1220.5000, 2, 2, 4)
SET IDENTITY_INSERT [dbo].[Divisions] ON 

INSERT [dbo].[Divisions] ([Department_code], [Department_name]) VALUES (1, N'Валютный-обменный отдел')
INSERT [dbo].[Divisions] ([Department_code], [Department_name]) VALUES (2, N'Отдел кредитования')
INSERT [dbo].[Divisions] ([Department_code], [Department_name]) VALUES (3, N'Отдел вкладов')
INSERT [dbo].[Divisions] ([Department_code], [Department_name]) VALUES (4, N'Обслуживающий отдел')
SET IDENTITY_INSERT [dbo].[Divisions] OFF
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (1, N'Баушева ', N'Розалия', N'Тимуровна', N'80291246538', CAST(0x00006F7500000000 AS DateTime), 1, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (2, N'Кашникова ', N'Мирослава', N'Валерьевна', N'80441237896', CAST(0x00007DA500000000 AS DateTime), 10, N'средне-специально             ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (3, N'Анникова ', N'Любовь', N'Прокофьевна', N'80254569872', CAST(0x00006A9E00000000 AS DateTime), 10, N'средне-специальное            ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (4, N'Шостенко ', N'Антонина', N'Герасимовна', N'80331245698', CAST(0x00006FC100000000 AS DateTime), 11, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (5, N'Курташкина', N'Анна', N'Петровна', N'80445694782', CAST(0x0000734800000000 AS DateTime), 2, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (6, N'Коромыслова ', N'Александра', N'Александровна', N'80253211459', CAST(0x000079F300000000 AS DateTime), 2, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (7, N'Шайна ', N'Надежда', N'Викторовна', N'80255323441', CAST(0x00006CBC00000000 AS DateTime), 4, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (8, N'Колпачёв', N'Михаил', N'Анатольевич', N'80291785272', CAST(0x0000758200000000 AS DateTime), 7, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (9, N'Никоненко ', N'Леонид', N'Александрович', N'80447037430', CAST(0x00006F2300000000 AS DateTime), 4, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (10, N'Чижиков', N'Виктор', N'Петрович', N'80333087122', CAST(0x0000653800000000 AS DateTime), 12, N'средне-специальное            ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (11, N'Гаранин', N'Потап', N'Евгеньевич', N'80296574523', CAST(0x0000805600000000 AS DateTime), 5, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (12, N'Актжанов', N'Михаил ', N'Касьянович', N'80331657432', CAST(0x000081E000000000 AS DateTime), 3, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (13, N'Счастливцев ', N'Павел', N'Якович', N'80291463528', CAST(0x0000870700000000 AS DateTime), 3, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (14, N'Колотушкин ', N'Петр', N'Васильевич', N'80339514965', CAST(0x000075C000000000 AS DateTime), 12, N'средне-специальное            ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (15, N'Никитина', N'Ольга', N'Викторовна', N'80441789635', CAST(0x00006D7200000000 AS DateTime), 7, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (16, N'Абакумов', N'Святослав', N'Михайлович', N'80294238975', CAST(0x00007AF900000000 AS DateTime), 9, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (17, N'Климцов', N'Эдуард ', N'Васильевич', N'80449235786', CAST(0x00006A7500000000 AS DateTime), 8, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (18, N'Хлебникова', N'Ирина', N'Викторовна', N'80291453786', CAST(0x0000851800000000 AS DateTime), 2, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (19, N'Дегтярева', N'Анжелика', N'Андреевна', N'80331657924', CAST(0x00007A8600000000 AS DateTime), 6, N'высшее                        ')
INSERT [dbo].[Employees] ([Employee_code], [Surname], [Name], [Patronymic], [Phone_number], [Date_of_Birth], [Job_code], [Qualification]) VALUES (20, N'Якубёнок', N'Виктория', N'Вадимовна', N'80255323441', CAST(0x00008E0C00000000 AS DateTime), 7, N'высшее                        ')
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (1, 1, N'Специалист по операционно-кассовой работе', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (2, 1, N'Валютный кассир', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (3, 1, N'Консультант', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (4, 2, N'Кредитный специалист', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (5, 2, N'Кредитный консультант', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (6, 3, N'Валютный консультант', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (7, 3, N'Специалист по выдаче вкладов', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (8, 4, N'Управляющий', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (9, 4, N'Программист', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (10, 4, N'Уборщик', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (11, 4, N'Бухгалтер', 8)
INSERT [dbo].[Employee's_position] ([Job_code], [Department_code], [Job_name], [Working_day]) VALUES (12, 4, N'Охранник', 8)
SET IDENTITY_INSERT [dbo].[Log] ON 

INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (1, 2000001, 5, CAST(0 AS Decimal(18, 0)), CAST(0x0000A7E5008F18D0 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (2, 2000001, 7, CAST(-1014 AS Decimal(18, 0)), CAST(0x0000A89A00000000 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (3, 2000002, 5, CAST(0 AS Decimal(18, 0)), CAST(0x0000A7E5009A5BA0 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (4, 2000003, 5, CAST(0 AS Decimal(18, 0)), CAST(0x0000A7E500A1C610 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (6, 2000004, 5, CAST(0 AS Decimal(18, 0)), CAST(0x0000A7E500ABA950 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (7, 2000005, 5, CAST(0 AS Decimal(18, 0)), CAST(0x0000A7E500B28720 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (8, 2000005, 7, CAST(-504 AS Decimal(18, 0)), CAST(0x0000A84000000000 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (9, 2000006, 5, CAST(0 AS Decimal(18, 0)), CAST(0x0000A7E6009F9390 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (10, 2000007, 5, CAST(0 AS Decimal(18, 0)), CAST(0x0000A7E600ABA950 AS DateTime))
INSERT [dbo].[Log] ([int_operation], [idacc], [operation_name], [amount_change], [date]) VALUES (11, 2000009, 5, CAST(0 AS Decimal(18, 0)), CAST(0x0000A8AC00F6717C AS DateTime))
SET IDENTITY_INSERT [dbo].[Log] OFF
SET IDENTITY_INSERT [dbo].[Operations] ON 

INSERT [dbo].[Operations] ([Operation_code], [Department_code], [Operation_name], [Discription]) VALUES (1, 1, N'Продажа валюты', NULL)
INSERT [dbo].[Operations] ([Operation_code], [Department_code], [Operation_name], [Discription]) VALUES (2, 1, N'Покупка валюты', NULL)
INSERT [dbo].[Operations] ([Operation_code], [Department_code], [Operation_name], [Discription]) VALUES (3, 2, N'Выдача кредита', NULL)
INSERT [dbo].[Operations] ([Operation_code], [Department_code], [Operation_name], [Discription]) VALUES (4, 2, N'Погашение кредита', NULL)
INSERT [dbo].[Operations] ([Operation_code], [Department_code], [Operation_name], [Discription]) VALUES (5, 3, N'Открытие вклада', NULL)
INSERT [dbo].[Operations] ([Operation_code], [Department_code], [Operation_name], [Discription]) VALUES (6, 3, N'Пополнение счета', NULL)
INSERT [dbo].[Operations] ([Operation_code], [Department_code], [Operation_name], [Discription]) VALUES (7, 3, N'Закрытие вклада', NULL)
INSERT [dbo].[Operations] ([Operation_code], [Department_code], [Operation_name], [Discription]) VALUES (8, 2, N'Закрытие кредита', NULL)
SET IDENTITY_INSERT [dbo].[Operations] OFF
SET IDENTITY_INSERT [dbo].[Region] ON 

INSERT [dbo].[Region] ([Region_code], [Region_name]) VALUES (1, N'Брестская ')
INSERT [dbo].[Region] ([Region_code], [Region_name]) VALUES (2, N'Витебская')
INSERT [dbo].[Region] ([Region_code], [Region_name]) VALUES (3, N'Гомельская')
INSERT [dbo].[Region] ([Region_code], [Region_name]) VALUES (4, N'Гродненская')
INSERT [dbo].[Region] ([Region_code], [Region_name]) VALUES (5, N'Могилевская')
INSERT [dbo].[Region] ([Region_code], [Region_name]) VALUES (6, N'Минск')
INSERT [dbo].[Region] ([Region_code], [Region_name]) VALUES (7, N'Минская')
SET IDENTITY_INSERT [dbo].[Region] OFF
INSERT [dbo].[Status] ([id_status], [name]) VALUES (1, N'активен   ')
INSERT [dbo].[Status] ([id_status], [name]) VALUES (2, N'пассивен  ')
INSERT [dbo].[Type_dep] ([id_type], [name_type]) VALUES (1, N'срочный             ')
INSERT [dbo].[Type_dep] ([id_type], [name_type]) VALUES (2, N'до востребования    ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2000, N'Интернет-депозит Тренд Отзывнойй                                                                    ', 840, 100.0000, CAST(1.10 AS Decimal(7, 2)), 90, 690, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2001, N'Интернет-депозит Тренд Отзывный                                                                     ', 978, 100.0000, CAST(1.10 AS Decimal(7, 2)), 90, 690, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2002, N'Интернет-депозит Тренд Отзывный                                                                     ', 643, 3000.0000, CAST(2.60 AS Decimal(7, 2)), 90, 690, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2003, N'Интернет-депозит Тренд Отзывный                                                                     ', 933, 50.0000, CAST(2.00 AS Decimal(7, 2)), 30, 270, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2004, N'Классик отзывный до года                                                                            ', 840, 100.0000, CAST(0.30 AS Decimal(7, 2)), 95, 185, 1, N'в днях! срочный                                                                                     ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2005, N'Классик отзывный до года                                                                            ', 978, 100.0000, CAST(0.30 AS Decimal(7, 2)), 95, 185, 1, N'в днях! срочный                                                                                     ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2006, N'Классик отзывной до года                                                                            ', 643, 5000.0000, CAST(2.50 AS Decimal(7, 2)), 95, 185, 1, N'в днях! срочный                                                                                     ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2007, N'Классик отзывный до года                                                                            ', 933, 150.0000, CAST(1.50 AS Decimal(7, 2)), 95, 185, 1, N'в днях! срочный                                                                                     ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2008, N'Классик отзывной свыше года                                                                         ', 840, 100.0000, CAST(1.60 AS Decimal(7, 2)), 95, 540, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2009, N'Классик отзывной свыше года                                                                         ', 978, 100.0000, CAST(1.60 AS Decimal(7, 2)), 95, 540, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2010, N'Классик отзывной свыше года                                                                         ', 643, 5000.0000, CAST(5.50 AS Decimal(7, 2)), 95, 540, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2011, N'Классик безотзывный до года                                                                         ', 840, 100.0000, CAST(0.70 AS Decimal(7, 2)), 90, 270, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2012, N'Классик безотзывный до года                                                                         ', 978, 100.0000, CAST(0.70 AS Decimal(7, 2)), 90, 270, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2013, N'Классик безотзывный до года                                                                         ', 643, 5000.0000, CAST(5.00 AS Decimal(7, 2)), 90, 270, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2014, N'Классик безотзывный до года                                                                         ', 933, 150.0000, CAST(3.20 AS Decimal(7, 2)), 90, 270, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2015, N'Классик безотзывный свыше года                                                                      ', 840, 100.0000, CAST(2.10 AS Decimal(7, 2)), 900, 1080, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2016, N'Классик безотзывный свыше года                                                                      ', 978, 100.0000, CAST(2.10 AS Decimal(7, 2)), 900, 1080, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2017, N'Классик безотзывный свыше года                                                                      ', 643, 5000.0000, CAST(4.70 AS Decimal(7, 2)), 900, 1080, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2018, N'Классик безотзывный свыше года                                                                      ', 933, 150.0000, CAST(9.70 AS Decimal(7, 2)), 540, 720, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2019, N'Классик отзывный на 35 дней                                                                         ', 933, 150.0000, CAST(1.00 AS Decimal(7, 2)), 1050, 1050, 1, N'дней, срочный                                                                                       ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2020, N'Классик почтовый отзывной                                                                           ', 933, 100.0000, CAST(5.50 AS Decimal(7, 2)), 360, 360, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2021, N'Классик почтовый безотзывный                                                                        ', 933, 100.0000, CAST(7.50 AS Decimal(7, 2)), 90, 360, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2022, N'Интернет-депозит – Тренд Безотзывный                                                                ', 840, 100.0000, CAST(1.40 AS Decimal(7, 2)), 0, 690, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2023, N'Интернет-депозит – Тренд Безотзывный                                                                ', 978, 100.0000, CAST(1.40 AS Decimal(7, 2)), 90, 690, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2024, N'Интернет-депозит – Тренд Безотзывный                                                                ', 643, 3000.0000, CAST(5.10 AS Decimal(7, 2)), 90, 690, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2025, N'Интернет-депозит – Тренд Безотзывный                                                                ', 933, 50.0000, CAST(3.50 AS Decimal(7, 2)), 30, 270, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2026, N'Классик безотзывный на 1 год                                                                        ', 933, 100.0000, CAST(7.50 AS Decimal(7, 2)), 30, 360, 1, N'срочный                                                                                             ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2027, N'Удаленный депозит                                                                                   ', 840, 1000.0000, CAST(1.50 AS Decimal(7, 2)), 90, 2040, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2028, N'Удаленный депозит                                                                                   ', 978, 1000.0000, CAST(1.50 AS Decimal(7, 2)), 90, 2040, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2029, N'Удаленный депозит                                                                                   ', 643, 100000.0000, CAST(3.50 AS Decimal(7, 2)), 90, 2040, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2030, N'Удаленный депозит                                                                                   ', 933, 10000.0000, CAST(3.25 AS Decimal(7, 2)), 95, 735, 1, N'в днях! для бизнеса                                                                                 ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2031, N'Срочный депозит в валюте (отзывный)                                                                 ', 840, 100.0000, CAST(0.50 AS Decimal(7, 2)), 30, 360, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2032, N'Срочный депозит в валюте (отзывный)                                                                 ', 978, 100.0000, CAST(0.50 AS Decimal(7, 2)), 30, 360, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2033, N'Срочный депозит в валюте (отзывный)                                                                 ', 643, 1000.0000, CAST(2.00 AS Decimal(7, 2)), 30, 360, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2035, N'Срочный депозит (безотзывный)                                                                       ', 840, 100.0000, CAST(1.00 AS Decimal(7, 2)), 30, 360, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2036, N'Срочный депозит (безотзывный)                                                                       ', 978, 100.0000, CAST(1.00 AS Decimal(7, 2)), 30, 360, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2037, N'Срочный депозит (безотзывный)                                                                       ', 643, 1000.0000, CAST(2.50 AS Decimal(7, 2)), 30, 360, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2038, N'Срочный депозит (безотзывный)                                                                       ', 933, 1000.0000, CAST(5.04 AS Decimal(7, 2)), 90, 180, 1, N'для бизнеса                                                                                         ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2039, N'До востребования                                                                                    ', 840, 1.0000, CAST(0.10 AS Decimal(7, 2)), 30, NULL, 2, N'до востребования                                                                                    ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2040, N'До востребования                                                                                    ', 978, 5.0000, CAST(0.10 AS Decimal(7, 2)), 30, NULL, 2, N'до востребования                                                                                    ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2041, N'До востребования                                                                                    ', 933, 1.0000, CAST(0.50 AS Decimal(7, 2)), 30, NULL, 2, N'до востребования                                                                                    ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2042, N'Пенсионный                                                                                          ', 933, 1.0000, CAST(0.50 AS Decimal(7, 2)), 30, NULL, 2, N'до востребования                                                                                    ')
INSERT [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code], [Name_type], [Currency_code], [Amount_min], [Rate], [Term_min], [Term_max], [Type_id], [Description]) VALUES (2043, N'Попечение                                                                                           ', 933, 0.5000, CAST(0.50 AS Decimal(7, 2)), 30, NULL, 2, N'до востребования                                                                                    ')
SET ANSI_PADDING ON

GO
/****** Object:  Index [DF_Passport]    Script Date: 13.12.2018 11:48:11 ******/
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Passport] UNIQUE NONCLUSTERED 
(
	[Passport_series] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DF_Passport_series]    Script Date: 13.12.2018 11:48:11 ******/
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Passport_series] UNIQUE NONCLUSTERED 
(
	[Passport_series] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Currency_exchange] ADD  CONSTRAINT [DF__Currency___Date___0FB750B3]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_Region] FOREIGN KEY([Region_code])
REFERENCES [dbo].[Region] ([Region_code])
GO
ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_City_Region]
GO
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_City] FOREIGN KEY([City])
REFERENCES [dbo].[City] ([City_code])
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_City]
GO
ALTER TABLE [dbo].[Credits_Deposits]  WITH CHECK ADD  CONSTRAINT [FK_Credits_Deposits_Clients] FOREIGN KEY([Client_code])
REFERENCES [dbo].[Clients] ([Client_code])
GO
ALTER TABLE [dbo].[Credits_Deposits] CHECK CONSTRAINT [FK_Credits_Deposits_Clients]
GO
ALTER TABLE [dbo].[Credits_Deposits]  WITH CHECK ADD  CONSTRAINT [FK_Credits_Deposits_Employees] FOREIGN KEY([Employee_code])
REFERENCES [dbo].[Employees] ([Employee_code])
GO
ALTER TABLE [dbo].[Credits_Deposits] CHECK CONSTRAINT [FK_Credits_Deposits_Employees]
GO
ALTER TABLE [dbo].[Credits_Deposits]  WITH CHECK ADD  CONSTRAINT [FK_Credits_Deposits_Status] FOREIGN KEY([status])
REFERENCES [dbo].[Status] ([id_status])
GO
ALTER TABLE [dbo].[Credits_Deposits] CHECK CONSTRAINT [FK_Credits_Deposits_Status]
GO
ALTER TABLE [dbo].[Credits_Deposits]  WITH CHECK ADD  CONSTRAINT [FK_Credits_Deposits_Types_of_credits_deposits1] FOREIGN KEY([Loan_Deposits_type_code])
REFERENCES [dbo].[Types_of_credits_deposits] ([Loan_Deposits_type_code])
GO
ALTER TABLE [dbo].[Credits_Deposits] CHECK CONSTRAINT [FK_Credits_Deposits_Types_of_credits_deposits1]
GO
ALTER TABLE [dbo].[Currency_exchange]  WITH CHECK ADD  CONSTRAINT [FK_Currency_exchange_Clients] FOREIGN KEY([Client_code])
REFERENCES [dbo].[Clients] ([Client_code])
GO
ALTER TABLE [dbo].[Currency_exchange] CHECK CONSTRAINT [FK_Currency_exchange_Clients]
GO
ALTER TABLE [dbo].[Currency_exchange]  WITH CHECK ADD  CONSTRAINT [FK_Currency_exchange_Currency] FOREIGN KEY([Currency_code])
REFERENCES [dbo].[Currency] ([Currency_code])
GO
ALTER TABLE [dbo].[Currency_exchange] CHECK CONSTRAINT [FK_Currency_exchange_Currency]
GO
ALTER TABLE [dbo].[Currency_exchange]  WITH CHECK ADD  CONSTRAINT [FK_Currency_exchange_Employees] FOREIGN KEY([Employee_code])
REFERENCES [dbo].[Employees] ([Employee_code])
GO
ALTER TABLE [dbo].[Currency_exchange] CHECK CONSTRAINT [FK_Currency_exchange_Employees]
GO
ALTER TABLE [dbo].[Currency_exchange]  WITH CHECK ADD  CONSTRAINT [FK_Currency_exchange_Operations] FOREIGN KEY([Operation_code])
REFERENCES [dbo].[Operations] ([Operation_code])
GO
ALTER TABLE [dbo].[Currency_exchange] CHECK CONSTRAINT [FK_Currency_exchange_Operations]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Employee's_position] FOREIGN KEY([Job_code])
REFERENCES [dbo].[Employee's_position] ([Job_code])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Employee's_position]
GO
ALTER TABLE [dbo].[Employee's_position]  WITH CHECK ADD  CONSTRAINT [FK_Employee's_position_Divisions] FOREIGN KEY([Department_code])
REFERENCES [dbo].[Divisions] ([Department_code])
GO
ALTER TABLE [dbo].[Employee's_position] CHECK CONSTRAINT [FK_Employee's_position_Divisions]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Credits_Deposits] FOREIGN KEY([idacc])
REFERENCES [dbo].[Credits_Deposits] ([Credit_Deposit_number])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Credits_Deposits]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Operations] FOREIGN KEY([operation_name])
REFERENCES [dbo].[Operations] ([Operation_code])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Operations]
GO
ALTER TABLE [dbo].[Operations]  WITH CHECK ADD  CONSTRAINT [FK_Operations_Divisions] FOREIGN KEY([Department_code])
REFERENCES [dbo].[Divisions] ([Department_code])
GO
ALTER TABLE [dbo].[Operations] CHECK CONSTRAINT [FK_Operations_Divisions]
GO
ALTER TABLE [dbo].[Types_of_credits_deposits]  WITH CHECK ADD  CONSTRAINT [FK_Types_of_credits_deposits_Currency] FOREIGN KEY([Currency_code])
REFERENCES [dbo].[Currency] ([Currency_code])
GO
ALTER TABLE [dbo].[Types_of_credits_deposits] CHECK CONSTRAINT [FK_Types_of_credits_deposits_Currency]
GO
ALTER TABLE [dbo].[Types_of_credits_deposits]  WITH CHECK ADD  CONSTRAINT [FK_Types_of_credits_deposits_Type_dep1] FOREIGN KEY([Type_id])
REFERENCES [dbo].[Type_dep] ([id_type])
GO
ALTER TABLE [dbo].[Types_of_credits_deposits] CHECK CONSTRAINT [FK_Types_of_credits_deposits_Type_dep1]
GO
ALTER TABLE [dbo].[Clients]  WITH NOCHECK ADD  CONSTRAINT [CK_Passport] CHECK NOT FOR REPLICATION (([Passport_series] like '[A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' OR [Passport_series] like '[А-Я][А-Я][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [CK_Passport]
GO
ALTER TABLE [dbo].[Currency_exchange]  WITH CHECK ADD  CONSTRAINT [CK_Currency_code] CHECK  (([Currency_Code]='980' OR [Currency_Code]='978' OR [Currency_Code]='840' OR [Currency_Code]='643' OR [Currency_Code]='933'))
GO
ALTER TABLE [dbo].[Currency_exchange] CHECK CONSTRAINT [CK_Currency_code]
GO
ALTER TABLE [dbo].[Currency_exchange]  WITH CHECK ADD  CONSTRAINT [CK_Currency_exchange] CHECK  (([Course]>(0)))
GO
ALTER TABLE [dbo].[Currency_exchange] CHECK CONSTRAINT [CK_Currency_exchange]
GO
ALTER TABLE [dbo].[Currency_exchange]  WITH CHECK ADD  CONSTRAINT [CK_Currency_Time] CHECK  (([The_time _of_the]>(0)))
GO
ALTER TABLE [dbo].[Currency_exchange] CHECK CONSTRAINT [CK_Currency_Time]
GO
ALTER TABLE [dbo].[Currency_exchange]  WITH CHECK ADD  CONSTRAINT [CK_Operation_code] CHECK  (([Operation_Code]='2' OR [Operation_Code]='1'))
GO
ALTER TABLE [dbo].[Currency_exchange] CHECK CONSTRAINT [CK_Operation_code]
GO
ALTER TABLE [dbo].[Employee's_position]  WITH CHECK ADD  CONSTRAINT [CK_Employee's_position] CHECK  (([Working_day]<=(10)))
GO
ALTER TABLE [dbo].[Employee's_position] CHECK CONSTRAINT [CK_Employee's_position]
GO
ALTER TABLE [dbo].[Types_of_credits_deposits]  WITH CHECK ADD  CONSTRAINT [CK_Types_of_credits_deposits] CHECK  (([Currency_Code]='980' OR [Currency_Code]='978' OR [Currency_Code]='840' OR [Currency_Code]='643' OR [Currency_Code]='933'))
GO
ALTER TABLE [dbo].[Types_of_credits_deposits] CHECK CONSTRAINT [CK_Types_of_credits_deposits]
GO
USE [master]
GO
ALTER DATABASE [OOP] SET  READ_WRITE 
GO
